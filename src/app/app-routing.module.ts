import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmRegisterComponent } from './authentication/confirm-register/confirm-register.component';
import { ConfirmRegisterGuard } from './authentication/confirm-register/confirm-register.guard';
import { ForgotPasswordComponent } from './authentication/forgot-password/forgot-password.component';
import { ForgotPasswordGuard } from './authentication/forgot-password/forgot-password.guard';
import { LoginComponent } from './authentication/login/login.component';
import { LoginGuard } from './authentication/login/login.guard';
import { RegisterComponent } from './authentication/register/register.component';
import { RegisterGuard } from './authentication/register/register.guard';
import { ResetPasswordComponent } from './authentication/reset-password/reset-password.component';
import { ResetPasswordGuard } from './authentication/reset-password/reset-password.guard';
import { GeneralComponent } from './general/general.component';
import { GeneralGuard } from './general/general.guard';

export const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'confirm-register', component: ConfirmRegisterComponent, canActivate: [ConfirmRegisterGuard], data: [window.location.search] },
  { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [ForgotPasswordGuard] },
  { path: 'general', component: GeneralComponent, canActivate: [GeneralGuard] },
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },
  { path: 'register', component: RegisterComponent, canActivate: [RegisterGuard] },
  { path: 'reset-password', component: ResetPasswordComponent, canActivate: [ResetPasswordGuard], data: [window.location.search] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule { }