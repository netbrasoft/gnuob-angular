import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgSelectModule } from '@ng-select/ng-select';
import { EditorModule } from '@tinymce/tinymce-angular';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { BlockUIModule } from 'ng-block-ui';
import { BlockUIHttpModule } from 'ng-block-ui/http';
import { GravatarModule } from 'ngx-gravatar';
import { IConfig, NgxMaskModule } from 'ngx-mask';
import { TreeviewModule } from 'ngx-treeview';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthenticationInterceptor } from './authentication/authentication.interceptor';
import { ConfirmRegisterComponent } from './authentication/confirm-register/confirm-register.component';
import { ForgotPasswordComponent } from './authentication/forgot-password/forgot-password.component';
import { LoginComponent } from './authentication/login/login.component';
import { PermissionComponent, PermissionModalComponent } from './authentication/permission/permission.component';
import { RegisterComponent } from './authentication/register/register.component';
import { ResetPasswordComponent } from './authentication/reset-password/reset-password.component';
import { RoleComponent, RoleModalComponent } from './authentication/role/role.component';
import { UserComponent, UserModalComponent } from './authentication/user/user.component';
import { CategoryComponent, CategoryModalComponent } from './category/category.component';
import { CustomerComponent, CustomerModalComponent } from './customer/customer.component';
import { GeneralComponent, ModalConfirmLogoutComponent } from './general/general.component';
import { OfferRecordComponent, OfferRecordModalComponent } from './offer/offer-record/offer-record.component';
import { OfferComponent, OfferModalComponent } from './offer/offer.component';
import { AttributeComponent, AttributeModalComponent } from './product/attribute/attribute.component';
import { ProductComponent, ProductModalComponent } from './product/product.component';
import { SettingComponent, SettingModalComponent } from './setting/setting.component';
import { ModalComponent, TypeConfirmDeleteModalComponent } from './type/abstract-type.component';
import { SortableHeaderDirective } from './type/directive/sortable-header.directive';
import { TypeComponentDirective } from './type/directive/type-component.directive';
import { TypeModalComponentDirective } from './type/directive/type-modal-component.directive';
import { QuestionComponent } from './type/question/question.component';

const maskConfig: Partial<IConfig> = {
  validation: false,
};

@NgModule({
  declarations: [
    AppComponent,
    AttributeComponent,
    AttributeModalComponent,
    CategoryComponent,
    CategoryModalComponent,
    ConfirmRegisterComponent,
    CustomerComponent,
    CustomerModalComponent,
    ForgotPasswordComponent,
    GeneralComponent,
    LoginComponent,
    ModalComponent,
    ModalConfirmLogoutComponent,
    OfferComponent,
    OfferModalComponent,
    OfferRecordComponent,
    OfferRecordModalComponent,
    PermissionComponent,
    PermissionModalComponent,
    ProductComponent,
    ProductModalComponent,
    QuestionComponent,
    RegisterComponent,
    ResetPasswordComponent,
    RoleComponent,
    RoleModalComponent,
    SettingComponent,
    SettingModalComponent,
    SortableHeaderDirective,
    TypeComponentDirective,
    TypeConfirmDeleteModalComponent,
    TypeModalComponentDirective,
    UserComponent,
    UserModalComponent,
  ],
  imports: [
    AngularDualListBoxModule,
    AppRoutingModule,
    BlockUIHttpModule.forRoot({ blockAllRequestsInProgress: true }),
    BlockUIModule.forRoot({ message: 'Please wait...' }),
    BrowserModule,
    EditorModule,
    FormsModule,
    GravatarModule,
    HttpClientModule,
    NgbModule,
    NgSelectModule,
    NgxMaskModule.forRoot(maskConfig),
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    TreeviewModule.forRoot(),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthenticationInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
