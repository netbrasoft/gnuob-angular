import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { User } from './user/user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private httpClient: HttpClient) { }

  public login(user: User): Observable<HttpResponse<any>> {
    return this.httpClient.post<HttpResponse<any>>(environment.endpoint + '/user/of/login', user, {
      params: {
        change_password: (!!user.changePassword ? 'true' : 'false')
      }
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public register(user: User): Observable<HttpResponse<any>> {
    return this.httpClient.post<HttpResponse<any>>(environment.endpoint + '/user/register', user, {
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public forgotPassword(user: User): Observable<HttpResponse<any>> {
    return this.httpClient.post<HttpResponse<any>>(environment.endpoint + '/user/forgot/password', user, {
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public confirmRegister(user: User, token: string): Observable<HttpResponse<any>> {
    return this.httpClient.post<HttpResponse<any>>(environment.endpoint + '/user/register/' + token, user, {
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public confirmForgotPassword(user: User, token: string): Observable<HttpResponse<any>> {
    return this.httpClient.post<HttpResponse<any>>(environment.endpoint + '/user/forgot/password/' + token, user, {
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public logout(user: User): Observable<HttpResponse<any>> {
    return this.httpClient.post<HttpResponse<any>>(environment.endpoint + '/user/of/logout', user, {
      headers: new HttpHeaders().set('Authorization', localStorage.getItem('token')),
    }).pipe(
      retry(3),
      map(response => response)
    );
  }
}
