import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { User } from '../user/user';

@Component({
  selector: 'app-confirm-register',
  templateUrl: './confirm-register.component.html',
  styleUrls: ['./confirm-register.component.sass']
})
export class ConfirmRegisterComponent implements OnInit {
  public confirmRegisterForm: FormGroup;
  public tokenControl: AbstractControl;
  public user: User = new User();
  public alert: string = undefined;
  public success: string = undefined;

  constructor(
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  public async ngOnInit(): Promise<void> {
    this.confirmRegisterForm = this.formBuilder.group({
      token: [await this.getToken(), [Validators.required]],
    });
    this.tokenControl = this.confirmRegisterForm.controls.token;
  }

  private async getToken(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this.route.data.subscribe(data => resolve(new URLSearchParams(data[0]).get('token')), error => reject(error));
    });
  }

  public async onSubmit(): Promise<void> {
    if (this.confirmRegisterForm.valid) {
      await this.confirmRegister();
    }
  }

  private async confirmRegister(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.authenticationService.confirmRegister(this.user, this.tokenControl.value)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then(() => {
      this.success = $localize`:@@confirmRegister:Please wait, we will redirecting you to the login page`;
      setTimeout(() => {
        this.router.navigate(['/login']);
      }, 1000);
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}
