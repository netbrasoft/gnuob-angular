import { TestBed } from '@angular/core/testing';
import { ConfirmRegisterGuard } from './confirm-register.guard';

describe('ConfirmRegisterGuard', () => {
  let guard: ConfirmRegisterGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ConfirmRegisterGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
