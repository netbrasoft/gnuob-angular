import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { User } from '../user/user';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.sass']
})
export class ForgotPasswordComponent implements OnInit {
  public forgotPasswordForm: FormGroup;
  public usernameControl: AbstractControl;
  public user: User = new User();
  public alert: string = undefined;
  public success: string = undefined;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
  ) { }

  public ngOnInit(): void {
    this.forgotPasswordForm = this.formBuilder.group({
      username: [this.user.username, [Validators.required, Validators.email, Validators.maxLength(127)]],
    });
    this.usernameControl = this.forgotPasswordForm.controls.username;
  }

  public async onSubmit(): Promise<void> {
    if (this.forgotPasswordForm.valid) {
      this.user.username = this.usernameControl.value;
      await this.forgotPassword();
    }
  }

  private async forgotPassword(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.authenticationService.forgotPassword(this.user)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then(() => {
      this.success = $localize`:@@forgotPassword:Please wait, we will redirecting you to the login page`;
      setTimeout(() => {
        this.router.navigate(['/login']);
      }, 1000);
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}
