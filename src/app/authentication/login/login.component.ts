import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';
import { User } from '../user/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public usernameControl: AbstractControl;
  public passwordControl: AbstractControl;
  public user: User = new User();
  public alert: string = undefined;
  public success: string = undefined;

  constructor(
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  public ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: [this.user.username, [Validators.required, Validators.email, Validators.maxLength(127)]],
      password: [this.user.password, [Validators.required, Validators.maxLength(127)]],
    });
    this.usernameControl = this.loginForm.controls.username;
    this.passwordControl = this.loginForm.controls.password;
  }

  public async onSubmit(): Promise<void> {
    if (this.loginForm.valid) {
      this.user.username = this.usernameControl.value;
      this.user.password = this.passwordControl.value;
      await this.login();
    }
  }

  private async login(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.authenticationService.login(this.user)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then((user: User) => {
      this.success = $localize`:@@login:Please wait, we will redirecting you to the general page`;
      localStorage.setItem('user', JSON.stringify(user));
      setTimeout(() => {
        this.router.navigate(['/general']);
      }, 1000);
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}
