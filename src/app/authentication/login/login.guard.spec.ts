import { TestBed } from '@angular/core/testing';
import { AppRoutingModule } from '../../app-routing.module';
import { LoginGuard } from './login.guard';

describe('LoginGuard', () => {
  let guard: LoginGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppRoutingModule,
      ]
    });
    guard = TestBed.inject(LoginGuard);
  });


  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
