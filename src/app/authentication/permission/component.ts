import { AbstractActiveType } from '../../type/abstract-active-type';
import { PermissionComponent } from './permission-component';
import { RoleComponent } from '../role/role-component';

export class Component extends AbstractActiveType {
  permissionComponents: PermissionComponent[];
  rolePermissionComponents: RoleComponent[];

  constructor() {
    super('component');
  }
}
