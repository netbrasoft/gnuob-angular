import { AbstractType } from '../../type/abstract-type';
import { Component } from './component';

export class PermissionComponent extends AbstractType {
  canEdit: boolean;
  isRequired: boolean;
  component: Component;

  constructor() {
    super('permissioncomponent');
  }
}
