import { AbstractType } from '../../type/abstract-type';
import { Role } from '../role/role';

export class PermissionRole extends AbstractType {
  role: Role;

  constructor() {
    super('permissionrole');
  }
}
