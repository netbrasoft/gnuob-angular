import { AbstractType } from '../../type/abstract-type';
import { User } from '../user/user';

export class PermissionUser extends AbstractType {
  user: User;

  constructor() {
    super('permissionuser');
  }
}
