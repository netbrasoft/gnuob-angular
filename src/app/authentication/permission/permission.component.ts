import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractTypeComponent, AbstractTypeModalComponent } from '../../type/abstract-type.component';
import { Column } from '../../type/directive/sortable-header.directive';
import { DualListQuestion } from '../../type/question/dual-list-question';
import { Question } from '../../type/question/question';
import { QuestionControlService } from '../../type/question/question-control.service';
import { TextboxQuestion } from '../../type/question/textbox-question';
import { TypeTopicService } from '../../type/type-topic.service';
import { TypeService } from '../../type/type.service';
import { Role } from '../role/role';
import { User } from '../user/user';
import { Permission } from './permission';
import { PermissionRole } from './permission-role';
import { PermissionUser } from './permission-user';

@Component({
  selector: 'app-permission-modal',
  templateUrl: '../../type/abstract-type-modal.component.html'
})
export class PermissionModalComponent<T extends Permission> extends AbstractTypeModalComponent<T> {
  private roles: Role[] = [];
  private users: User[] = [];
  public permission: Question<string>[] = [];
  public permissionRoles: Question<string>[] = [];
  public permissionUsers: Question<string>[] = [];

  constructor(
    private questionControlService: QuestionControlService,
    private roleService: TypeService<Role>,
    private userService: TypeService<User>,
  ) {
    super();
  }

  public async onInit(): Promise<void> {
    await this.findAllRoles();
    await this.findAllUsers();
    this.permission = [
      new TextboxQuestion({
        key: 'name',
        label: 'Name',
        value: this.type.name,
        required: true,
        validators: [Validators.required, Validators.maxLength(125)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'code',
        label: 'Code',
        value: this.type.code,
        required: true,
        validators: [Validators.required, Validators.maxLength(255)],
        size: 6,
        order: 2
      }),
      new TextboxQuestion({
        key: 'description',
        label: 'Note',
        value: this.type.description,
        validators: [Validators.maxLength(1024)],
        order: 3
      })
    ];
    this.permissionRoles = [
      new DualListQuestion({
        key: 'permissionRoles',
        label: 'Roles',
        source: this.roles,
        destination: this.type.permissionRoles?.map(pr => pr.role),
      })
    ];
    this.permissionUsers = [
      new DualListQuestion({
        key: 'permissionUsers',
        label: 'Users',
        source: this.users,
        destination: this.type.permissionUsers?.map(ru => ru.user),
      })
    ];
    this.navs = [{
      title: 'General Information',
      questions: this.permission
    }, {
      title: 'Roles',
      questions: this.permissionRoles
    }, {
      title: 'Users',
      questions: this.permissionUsers
    }];
    let questions = [];
    this.navs.forEach(nav => questions = questions.concat(nav.questions));
    this.typeForm = this.questionControlService.toFormGroup(questions);
    this.ready = true;
  }

  public onSubmit(): boolean {
    if (this.typeForm.valid) {
      this.type.name = this.typeForm.controls.name.value;
      this.type.code = this.typeForm.controls.code.value;
      this.type.description = this.typeForm.controls.description.value;
      this.type.permissionRoles = [];
      this.type.permissionUsers = [];
      this.permissionRoles.forEach(pr => {
        pr.destination.forEach(role => {
          const roleUser: PermissionRole = new PermissionRole();
          roleUser.role = role;
          this.type.permissionRoles.push(roleUser);
        });
      });
      this.permissionUsers.forEach(pu => {
        pu.destination.forEach(user => {
          const permissionUser: PermissionUser = new PermissionUser();
          permissionUser.user = user;
          this.type.permissionUsers.push(permissionUser);
        });
      });
      return true;
    }
    return false;
  }

  private async findAllRoles(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.roleService.findAll(1, 100, '', ['name'], new Role(true))
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: Role[] }) => {
      this.roles = data.types;
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }

  private async findAllUsers(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.userService.findAll(1, 100, '', ['name'], new User(true))
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: User[] }) => {
      this.users = data.types;
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}

@Component({
  selector: 'app-permission',
  templateUrl: '../../type/abstract-type.component.html',
  styleUrls: ['./permission.component.sass']
})
export class PermissionComponent extends AbstractTypeComponent<Permission> {
  constructor(
    formBuilder: FormBuilder,
    modalService: NgbModal,
    typeService: TypeService<Permission>,
    typServiceTopic: TypeTopicService,
  ) {
    super(formBuilder, modalService, typeService, typServiceTopic, new Permission(), 'Permission Details');
  }

  public doGetColumns(): Column<User>[] {
    return [
      { key: 'name', title: 'Name', type: 'string' },
      { key: 'code', title: 'Code', type: 'string' },
      { key: 'description', title: 'Note', type: 'string' },
    ];
  }

  public doGetModal() {
    return PermissionModalComponent;
  }
}
