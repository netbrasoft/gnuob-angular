import { AbstractActiveType } from '../../type/abstract-active-type';
import { PermissionComponent } from './permission-component';
import { PermissionRole } from './permission-role';
import { PermissionUser } from './permission-user';

export class Permission extends AbstractActiveType {
  permissionRoles: PermissionRole[] = [];
  permissionUsers: PermissionUser[] = [];
  permissionComponents: PermissionComponent[] = [];

  constructor(active?: boolean) {
    super('permission');
    super.active = active;
  }
}
