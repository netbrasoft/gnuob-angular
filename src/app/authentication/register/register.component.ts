import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { passwordMismatchValidator } from '../../utils';
import { AuthenticationService } from '../authentication.service';
import { User } from '../user/user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  public firstNameControl: AbstractControl;
  public lastNameControl: AbstractControl;
  public usernameControl: AbstractControl;
  public passwordControl: AbstractControl;
  public repeatPasswordControl: AbstractControl;
  public user: User = new User();
  public alert: string = undefined;
  public success: string = undefined;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
  ) { }

  public ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      firstName: [this.user.firstName, [Validators.required, Validators.maxLength(25)]],
      lastName: [this.user.lastName, [Validators.required, Validators.maxLength(25)]],
      username: [this.user.username, [Validators.required, Validators.email, Validators.maxLength(127)]],
      password: [this.user.password, [Validators.required, Validators.pattern('^(?=.*\\d).{4,8}$'), Validators.maxLength(127)]],
      repeatPassword: [this.user.repeatPassword, [Validators.required, Validators.maxLength(127)]],
    }, {
      validators: [passwordMismatchValidator]
    });
    this.firstNameControl = this.registerForm.controls.firstName;
    this.lastNameControl = this.registerForm.controls.lastName;
    this.usernameControl = this.registerForm.controls.username;
    this.passwordControl = this.registerForm.controls.password;
    this.repeatPasswordControl = this.registerForm.controls.repeatPassword;
  }

  public async onSubmit(): Promise<void> {
    if (this.registerForm.valid) {
      this.user.firstName = this.firstNameControl.value;
      this.user.lastName = this.lastNameControl.value;
      this.user.username = this.usernameControl.value;
      this.user.password = this.passwordControl.value;
      this.user.repeatPassword = this.repeatPasswordControl.value;
      await this.register();
    }
  }

  private async register(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.authenticationService.register(this.user)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then(() => {
      this.success = $localize`:@@register:Please wait, we will redirecting you to the login page`;
      setTimeout(() => {
        this.router.navigate(['/login']);
      }, 1000);
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}
