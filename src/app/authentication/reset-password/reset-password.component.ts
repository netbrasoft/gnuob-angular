import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { passwordMismatchValidator } from '../../utils';
import { AuthenticationService } from '../authentication.service';
import { User } from '../user/user';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.sass']
})
export class ResetPasswordComponent implements OnInit {
  public resetPasswordForm: FormGroup;
  public tokenControl: AbstractControl;
  public passwordControl: AbstractControl;
  public repeatPasswordControl: AbstractControl;
  public user: User = new User();
  public alert: string = undefined;
  public success: string = undefined;

  constructor(
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  public async ngOnInit(): Promise<void> {
    this.resetPasswordForm = this.formBuilder.group({
      token: [await this.getToken(), [Validators.required]],
      password: [this.user.password, [Validators.required, Validators.pattern('^(?=.*\\d).{4,8}$'), Validators.maxLength(127)]],
      repeatPassword: [this.user.repeatPassword, [Validators.required, Validators.maxLength(127)]],
    }, {
      validators: [passwordMismatchValidator]
    });
    this.tokenControl = this.resetPasswordForm.controls.token;
    this.passwordControl = this.resetPasswordForm.controls.password;
    this.repeatPasswordControl = this.resetPasswordForm.controls.repeatPassword;
  }

  private async getToken(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this.route.data.subscribe(data => resolve(new URLSearchParams(data[0]).get('token')), error => reject(error));
    });
  }

  public async onSubmit(): Promise<void> {
    if (this.resetPasswordForm.valid) {
      this.user.password = this.passwordControl.value;
      this.user.repeatPassword = this.repeatPasswordControl.value;
      await this.confirmForgotPassword();
    }
  }

  private async confirmForgotPassword(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.authenticationService.confirmForgotPassword(this.user, this.tokenControl.value)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then(() => {
      this.success = $localize`:@@confirmForgotPassword:Please wait, we will redirecting you to the login page`;
      setTimeout(() => {
        this.router.navigate(['/login']);
      }, 1000);
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}
