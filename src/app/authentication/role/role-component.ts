import { AbstractType } from '../../type/abstract-type';
import { Component } from '../permission/component';

export class RoleComponent extends AbstractType {
  canEdit: boolean;
  isRequired: boolean;
  component: Component;

  constructor() {
    super('rolecomponent');
  }
}
