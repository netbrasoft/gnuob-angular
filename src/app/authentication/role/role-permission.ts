import { AbstractType } from '../../type/abstract-type';
import { Permission } from '../permission/permission';
import { RoleComponent } from './role-component';

export class RolePermission extends AbstractType {
  canCreate: boolean;
  canRead: boolean;
  canUpdate: boolean;
  canDelete: boolean;
  permission: Permission;
  roleComponents: RoleComponent[] = [];

  constructor() {
    super('rolepermission');
  }
}
