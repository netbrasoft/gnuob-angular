import { AbstractType } from '../../type/abstract-type';
import { User } from '../user/user';

export class RoleUser extends AbstractType {
  user: User;

  constructor() {
    super('roleuser');
  }
}
