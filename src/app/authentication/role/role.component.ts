import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractTypeComponent, AbstractTypeModalComponent } from '../../type/abstract-type.component';
import { Column } from '../../type/directive/sortable-header.directive';
import { DualListQuestion } from '../../type/question/dual-list-question';
import { Question } from '../../type/question/question';
import { QuestionControlService } from '../../type/question/question-control.service';
import { TextboxQuestion } from '../../type/question/textbox-question';
import { TypeTopicService } from '../../type/type-topic.service';
import { TypeService } from '../../type/type.service';
import { Permission } from '../permission/permission';
import { User } from '../user/user';
import { Role } from './role';
import { RolePermission } from './role-permission';
import { RoleUser } from './role-user';

@Component({
  selector: 'app-role-modal',
  templateUrl: '../../type/abstract-type-modal.component.html'
})
export class RoleModalComponent<T extends Role> extends AbstractTypeModalComponent<T> {
  private permissions: Permission[] = [];
  private users: User[] = [];
  public role: Question<string>[] = [];
  public roleUsers: Question<string>[] = [];
  public rolePermissions: Question<string>[] = [];

  constructor(
    private permissionService: TypeService<Permission>,
    private questionControlService: QuestionControlService,
    private userService: TypeService<User>,
  ) {
    super();
  }

  public async onInit(): Promise<void> {
    await this.findAllPermissions();
    await this.findAllUsers();
    this.role = [
      new TextboxQuestion({
        key: 'name',
        label: 'Name',
        value: this.type.name,
        required: true,
        validators: [Validators.required, Validators.maxLength(125)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'code',
        label: 'Code',
        value: this.type.code,
        required: true,
        validators: [Validators.required, Validators.maxLength(255)],
        size: 6,
        order: 2
      }),
      new TextboxQuestion({
        key: 'description',
        label: 'Note',
        value: this.type.description,
        validators: [Validators.maxLength(1024)],
        order: 3
      })
    ];
    this.roleUsers = [
      new DualListQuestion({
        key: 'roleUsers',
        label: 'Users',
        source: this.users,
        destination: this.type.roleUsers?.map(ru => ru.user),
      })
    ];
    this.rolePermissions = [
      new DualListQuestion({
        key: 'rolePermissions',
        label: 'Permissions',
        source: this.permissions,
        destination: this.type.rolePermissions?.map(rp => rp.permission),
      })
    ];
    this.navs = [{
      title: 'General Information',
      questions: this.role
    }, {
      title: 'Users',
      questions: this.roleUsers
    }, {
      title: 'Permissions',
      questions: this.rolePermissions
    }];
    let questions = [];
    this.navs.forEach(nav => questions = questions.concat(nav.questions));
    this.typeForm = this.questionControlService.toFormGroup(questions);
    this.ready = true;
  }

  public onSubmit(): boolean {
    if (this.typeForm.valid) {
      this.type.name = this.typeForm.controls.name.value;
      this.type.code = this.typeForm.controls.code.value;
      this.type.description = this.typeForm.controls.description.value;
      this.type.roleUsers = [];
      this.type.rolePermissions = [];
      this.roleUsers.forEach(ur => {
        ur.destination.forEach(user => {
          const roleUser: RoleUser = new RoleUser();
          roleUser.user = user;
          this.type.roleUsers.push(roleUser);
        });
      });
      this.rolePermissions.forEach(rp => {
        rp.destination.forEach(permission => {
          const rolePermission: RolePermission = new RolePermission();
          rolePermission.permission = permission;
          this.type.rolePermissions.push(rolePermission);
        });
      });
      return true;
    }
    return false;
  }

  private async findAllPermissions(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.permissionService.findAll(1, 100, '', ['name'], new Permission(true))
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: Permission[] }) => {
      this.permissions = data.types;
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }

  private async findAllUsers(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.userService.findAll(1, 100, '', ['name'], new User(true))
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: User[] }) => {
      this.users = data.types;
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}

@Component({
  selector: 'app-role',
  templateUrl: '../../type/abstract-type.component.html',
  styleUrls: ['./role.component.sass']
})
export class RoleComponent extends AbstractTypeComponent<Role> {
  constructor(
    formBuilder: FormBuilder,
    modalService: NgbModal,
    typeService: TypeService<Role>,
    typServiceTopic: TypeTopicService,
  ) {
    super(formBuilder, modalService, typeService, typServiceTopic, new Role(), 'Role Details');
  }

  public doGetColumns(): Column<User>[] {
    return [
      { key: 'name', title: 'Name', type: 'string' },
      { key: 'code', title: 'Code', type: 'string' },
      { key: 'description', title: 'Note', type: 'string' },
    ];
  }

  public doGetModal() {
    return RoleModalComponent;
  }
}
