import { AbstractActiveType } from '../../type/abstract-active-type';
import { RolePermission } from './role-permission';
import { RoleUser } from './role-user';

export class Role extends AbstractActiveType {
  rolePermissions: RolePermission[] = [];
  roleUsers: RoleUser[] = [];

  constructor(active?: boolean) {
    super('role');
    super.active = active;
  }
}
