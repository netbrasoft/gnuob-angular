import { AbstractType } from '../../type/abstract-type';
import { Permission } from '../permission/permission';

export class UserPermission extends AbstractType {
  canCreate: boolean;
  canRead: boolean;
  canUpdate: boolean;
  canDelete: boolean;
  permission: Permission;

  constructor() {
    super('userpermission');
  }
}
