import { AbstractType } from '../../type/abstract-type';
import { Role } from '../role/role';

export class UserRole extends AbstractType {
  role: Role;

  constructor() {
    super('userrole');
  }
}
