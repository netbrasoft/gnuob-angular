import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractTypeComponent, AbstractTypeModalComponent } from '../../type/abstract-type.component';
import { Column } from '../../type/directive/sortable-header.directive';
import { DualListQuestion } from '../../type/question/dual-list-question';
import { Question } from '../../type/question/question';
import { QuestionControlService } from '../../type/question/question-control.service';
import { TextboxQuestion } from '../../type/question/textbox-question';
import { TypeTopicService } from '../../type/type-topic.service';
import { TypeService } from '../../type/type.service';
import { Permission } from '../permission/permission';
import { Role } from '../role/role';
import { User } from './user';
import { UserPermission } from './user-permission';
import { UserRole } from './user-role';

@Component({
  selector: 'app-user-modal',
  templateUrl: '../../type/abstract-type-modal.component.html'
})
export class UserModalComponent<T extends User> extends AbstractTypeModalComponent<T> {
  private roles: Role[] = [];
  private permissions: Permission[] = [];
  public user: Question<string>[] = [];
  public userRoles: Question<string>[] = [];
  public userPermissions: Question<string>[] = [];

  constructor(
    private permissionService: TypeService<Permission>,
    private questionControlService: QuestionControlService,
    private roleService: TypeService<Role>,
  ) {
    super();
  }

  public async onInit(): Promise<void> {
    await this.findAllRoles();
    await this.findAllPermissions();
    this.user = [
      new TextboxQuestion({
        key: 'firstName',
        label: 'Firts Name',
        value: this.type.firstName,
        required: true,
        validators: [Validators.required, Validators.maxLength(25)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'lastName',
        label: 'Last Name',
        value: this.type.lastName,
        required: true,
        validators: [Validators.required, Validators.maxLength(25)],
        size: 6,
        order: 2
      }),
      new TextboxQuestion({
        key: 'username',
        label: 'Email Address',
        value: { value: this.type?.username, disabled: !!this.type?.username },
        required: true,
        validators: [Validators.required, Validators.email, Validators.maxLength(127)],
        size: 6,
        order: 3
      }),
      new TextboxQuestion({
        key: 'description',
        label: 'Note',
        value: this.type.description,
        validators: [Validators.maxLength(1024)],
        order: 3
      }),
    ];
    this.userRoles = [
      new DualListQuestion({
        key: 'userRoles',
        label: 'Roles',
        source: this.roles,
        destination: this.type.userRoles?.map(ru => ru.role),
      })
    ];
    this.userPermissions = [
      new DualListQuestion({
        key: 'userPermissions',
        label: 'Permissions',
        source: this.permissions,
        destination: this.type.userPermissions?.map(rp => rp.permission),
      })
    ];
    this.navs = [{
      title: 'General Information',
      questions: this.user
    }, {
      title: 'Roles',
      questions: this.userRoles
    }, {
      title: 'Permissions',
      questions: this.userPermissions
    }];
    let questions = [];
    this.navs.forEach(nav => questions = questions.concat(nav.questions));
    this.typeForm = this.questionControlService.toFormGroup(questions);
    this.ready = true;
  }

  public onSubmit(): boolean {
    if (this.typeForm.valid) {
      this.type.firstName = this.typeForm.controls.firstName.value;
      this.type.lastName = this.typeForm.controls.lastName.value;
      this.type.username = this.typeForm.controls.username.value;
      this.type.userRoles = [];
      this.type.userPermissions = [];
      this.userRoles.forEach(ur => {
        ur.destination.forEach(role => {
          const userRole: UserRole = new UserRole();
          userRole.role = role;
          this.type.userRoles.push(userRole);
        });
      });
      this.userPermissions.forEach(rp => {
        rp.destination.forEach(permission => {
          const userPermission: UserPermission = new UserPermission();
          userPermission.permission = permission;
          this.type.userPermissions.push(userPermission);
        });
      });
      return true;
    }
    return false;
  }

  private async findAllRoles(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.roleService.findAll(1, 100, '', ['name'], new Role(true))
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: Role[] }) => {
      this.roles = data.types;
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }

  private async findAllPermissions(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.permissionService.findAll(1, 100, '', ['name'], new Permission(true))
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: Permission[] }) => {
      this.permissions = data.types;
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}

@Component({
  selector: 'app-user',
  templateUrl: '../../type/abstract-type.component.html',
  styleUrls: ['./user.component.sass']
})
export class UserComponent extends AbstractTypeComponent<User> {
  constructor(
    formBuilder: FormBuilder,
    modalService: NgbModal,
    typeService: TypeService<User>,
    typeServiceTopic: TypeTopicService,
  ) {
    super(formBuilder, modalService, typeService, typeServiceTopic, new User(), 'User Details');
  }

  public doGetColumns(): Column<User>[] {
    return [
      { key: 'firstName', title: 'First Name', type: 'string' },
      { key: 'lastName', title: 'Last Name', type: 'string' },
      { key: 'username', title: 'Username', type: 'string' },
    ];
  }

  public doGetModal() {
    return UserModalComponent;
  }
}
