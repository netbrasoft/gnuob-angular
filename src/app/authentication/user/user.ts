import { AbstractActiveType } from '../../type/abstract-active-type';
import { UserPermission } from './user-permission';
import { UserRole } from './user-role';

export class User extends AbstractActiveType {
  changePassword: boolean;
  mobile: boolean;
  online: boolean;
  root: boolean;
  email: string;
  firstName: string;
  lastName: string;
  username: string;
  mobileIpAddress: string;
  mobileManufacturer: string;
  mobileMaxDownload: string;
  mobileModel: string;
  mobileNetworkType: string;
  mobilePackageName: string;
  mobilePlatform: string;
  mobileSerial: string;
  mobileSignal: string;
  mobileUuid: string;
  mobileVersion: string;
  mobileVersionCode: string;
  mobileVersionNumber: string;
  password: string;
  repeatPassword: string;
  token: string;
  cookie: string;
  userPermissions: UserPermission[] = [];
  userRoles: UserRole[] = [];

  constructor(active?: boolean) {
    super('user');
    super.active = active;
  }
}
