import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractTypeComponent, AbstractTypeModalComponent } from '../type/abstract-type.component';
import { Column } from '../type/directive/sortable-header.directive';
import { Question } from '../type/question/question';
import { QuestionControlService } from '../type/question/question-control.service';
import { SelectQuestion } from '../type/question/select-question';
import { TextboxQuestion } from '../type/question/textbox-question';
import { TypeTopicService } from '../type/type-topic.service';
import { TypeService } from '../type/type.service';
import { Category } from './category';

@Component({
  selector: 'app-category-modal',
  templateUrl: '../type/abstract-type-modal.component.html'
})
export class CategoryModalComponent<T extends Category> extends AbstractTypeModalComponent<T> {
  private categories: Category[] = [];
  private categoryOptions: any[] = [];
  public category: Question<string>[] = [];

  constructor(
    private categoryService: TypeService<Category>,
    private questionControlService: QuestionControlService,
  ) {
    super();
  }

  public async onInit(): Promise<void> {
    await this.findAllCategories();
    this.category = [
      new TextboxQuestion({
        key: 'name',
        label: 'Name',
        value: this.type.name,
        required: true,
        validators: [Validators.required, Validators.maxLength(125)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'description',
        label: 'Note',
        value: this.type.description,
        validators: [Validators.maxLength(1024)],
        order: 3
      }),
      new TextboxQuestion({
        key: 'position',
        label: 'Position',
        value: this.type.position,
        validators: [Validators.maxLength(3)],
        size: 6,
        type: 'number',
        order: 4
      }),
      new SelectQuestion({
        key: 'parent',
        label: 'Parent',
        value: this.type?.parent?.id,
        size: 6,
        options: this.categoryOptions,
        order: 5
      }),
    ];
    this.navs = [{
      title: 'General Information',
      questions: this.category
    }];
    let questions = [];
    this.navs.forEach(nav => questions = questions.concat(nav.questions));
    this.typeForm = this.questionControlService.toFormGroup(questions);
    this.ready = true;
  }

  private async findAllCategories(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.categoryService.findAll(1, 100, 'asc', ['code'], new Category(true))
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: Category[] }) => {
      this.categories = data.types;
      this.categories.forEach(c => {
        this.categoryOptions.push({
          value: c.id,
          label: c.code
        });
      });
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }

  public onSubmit(): boolean {
    if (this.typeForm.valid) {
      this.type.name = this.typeForm.controls.name.value;
      this.type.description = this.typeForm.controls.description.value;
      this.type.position = this.typeForm.controls.position.value;
      this.type.parent = this.categories.find(c => c.id === +this.typeForm.controls.parent?.value?.value);
      return true;
    }
    return false;
  }
}

@Component({
  selector: 'app-category',
  templateUrl: '../type/abstract-type.component.html',
  styleUrls: ['./category.component.sass']
})
export class CategoryComponent extends AbstractTypeComponent<Category> {
  constructor(
    formBuilder: FormBuilder,
    modalService: NgbModal,
    typeService: TypeService<Category>,
    typServiceTopic: TypeTopicService,
  ) {
    super(formBuilder, modalService, typeService, typServiceTopic, new Category(), 'Category Details');
  }

  public doGetColumns(): Column<Category>[] {
    return [
      { key: 'name', title: 'Name', type: 'string' },
      { key: 'code', title: 'Code', type: 'string' },
      { key: 'position', title: 'Position', type: 'number' },
      { key: 'parent.name', title: 'Parent Name', type: 'string' },
      { key: 'parent.code', title: 'Parent Code', type: 'string' },
      { key: 'parent.position', title: 'Parent Position', type: 'number' },
    ];
  }

  public doGetModal() {
    return CategoryModalComponent;
  }
}
