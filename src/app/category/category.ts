import { AbstractActiveType } from '../type/abstract-active-type';

export class Category extends AbstractActiveType {
  position: number;
  parent: Category;

  constructor(active?: boolean) {
    super('category');
    super.active = active;
  }
}
