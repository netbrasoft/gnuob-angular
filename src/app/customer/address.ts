import { AbstractType } from '../type/abstract-type';

export class Address extends AbstractType {
  cityName: string;
  country: string;
  phone: string;
  postalCode: string;
  stateOrProvince: string;
  street1: string;
  street2: string;
  facebook: string;
  fax: string;
  linkedin: string;
  mobilePhone: string;
  website: string;

  constructor() {
    super('address');
  }
}
