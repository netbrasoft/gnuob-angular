import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractTypeComponent, AbstractTypeModalComponent } from '../type/abstract-type.component';
import { Column } from '../type/directive/sortable-header.directive';
import { Question } from '../type/question/question';
import { QuestionControlService } from '../type/question/question-control.service';
import { TextboxQuestion } from '../type/question/textbox-question';
import { TypeTopicService } from '../type/type-topic.service';
import { TypeService } from '../type/type.service';
import { Customer } from './customer';
import * as moment from 'moment';

@Component({
  selector: 'app-customer-modal',
  templateUrl: '../type/abstract-type-modal.component.html'
})
export class CustomerModalComponent<T extends Customer> extends AbstractTypeModalComponent<T> {
  public customer: Question<string>[] = [];
  public address: Question<string>[] = [];

  constructor(
    private questionControlService: QuestionControlService,
  ) {
    super();
    moment.updateLocale(moment.locale(), { invalidDate: '' });
  }

  public async onInit(): Promise<void> {
    this.customer = [
      new TextboxQuestion({
        key: 'firstName',
        label: 'First Name',
        value: this.type.firstName,
        required: true,
        validators: [Validators.required, Validators.maxLength(25)],
        size: 4,
        order: 1
      }),
      new TextboxQuestion({
        key: 'middleName',
        label: 'Middle Name',
        value: this.type.middleName,
        validators: [Validators.maxLength(25)],
        size: 4,
        order: 2
      }),
      new TextboxQuestion({
        key: 'lastName',
        label: 'Last Name',
        value: this.type.lastName,
        required: true,
        validators: [Validators.required, Validators.maxLength(25)],
        size: 4,
        order: 3
      }),
      new TextboxQuestion({
        key: 'salutation',
        label: 'Salutation',
        value: this.type.salutation,
        validators: [Validators.maxLength(25)],
        size: 6,
        order: 4
      }),
      new TextboxQuestion({
        key: 'suffix',
        label: 'Suffix',
        value: this.type.suffix,
        validators: [Validators.maxLength(12)],
        size: 6,
        order: 5
      }),
      new TextboxQuestion({
        key: 'contactPhone',
        label: 'Primary Contact',
        value: this.type.contactPhone,
        size: 6,
        mask: '(00) 0000-0000 || (00) 00000-0000',
        validators: [Validators.pattern('^[1-9]{2}(?:[2-8]|9[1-9])[0-9]{3}[0-9]{4}$'), Validators.maxLength(17)],
        order: 6
      }),
      new TextboxQuestion({
        key: 'username',
        label: 'User Account',
        value: { value: this.type?.user?.username, disabled: true },
        size: 6,
        order: 7
      }),
      new TextboxQuestion({
        key: 'code',
        label: 'Customer Number',
        value: this.type.code,
        validators: [Validators.required, Validators.email, Validators.maxLength(255)],
        size: 6,
        order: 8
      }),
      new TextboxQuestion({
        key: 'dateOfBirth',
        label: 'Date of Birth',
        value: !!this.type.dateOfBirth ? moment(this.type.dateOfBirth).format('DDMMYYYY') : '',
        mask: '00-00-0000',
        validators: [Validators.pattern('^(0[1-9]|1[0-9]|2[0-9]|3[01])(0[1-9]|1[012])[0-9]{4}$'), Validators.maxLength(8)],
        size: 6,
        order: 9
      }),
      new TextboxQuestion({
        key: 'buyerEmail',
        label: 'Buyer Email Address',
        value: this.type.buyerEmail,
        required: true,
        validators: [Validators.required, Validators.email, Validators.maxLength(127)],
        size: 6,
        order: 10
      }),
      new TextboxQuestion({
        key: 'buyerMarketingEmail',
        label: 'Marketing Email Address',
        value: this.type.buyerMarketingEmail,
        validators: [Validators.email, Validators.maxLength(127)],
        size: 6,
        order: 11
      }),
      new TextboxQuestion({
        key: 'payer',
        label: 'Payer Email Address',
        value: this.type.payer,
        validators: [Validators.email, Validators.maxLength(127)],
        size: 6,
        order: 12
      }),
      new TextboxQuestion({
        key: 'payerId',
        label: 'Payer Id',
        value: this.type.payerId,
        validators: [Validators.maxLength(127)],
        size: 6,
        order: 13
      }),
      new TextboxQuestion({
        key: 'payerBusiness',
        label: 'Payer Business',
        value: this.type.payerBusiness,
        validators: [Validators.maxLength(127)],
        size: 6,
        order: 14
      }),
      new TextboxQuestion({
        key: 'payerStatus',
        label: 'Status',
        value: this.type.payerStatus,
        validators: [Validators.maxLength(25)],
        size: 6,
        order: 15
      }),
      new TextboxQuestion({
        key: 'taxId',
        label: 'Tax Id',
        value: this.type.taxId,
        validators: [Validators.maxLength(25)],
        size: 6,
        order: 16
      }),
      new TextboxQuestion({
        key: 'taxIdType',
        label: 'Tax Id Type',
        value: this.type.taxIdType,
        validators: [Validators.maxLength(25)],
        size: 6,
        order: 17
      }),
    ];
    this.address = [
      new TextboxQuestion({
        key: 'phone',
        label: 'Phone',
        value: this.type?.address?.phone,
        mask: '(00) 0000-0000 || (00) 00000-0000',
        validators: [Validators.pattern('^[1-9]{2}(?:[2-8]|9[1-9])[0-9]{3}[0-9]{4}$'), Validators.maxLength(17)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'street1',
        label: 'Address',
        value: this.type?.address?.street1,
        validators: [Validators.maxLength(35)],
        size: 6,
        order: 2
      }),
      new TextboxQuestion({
        key: 'mobilePhone',
        label: 'Mobile Phone',
        value: this.type?.address?.mobilePhone,
        mask: '(00) 0000-0000 || (00) 00000-0000',
        validators: [Validators.pattern('^[1-9]{2}(?:[2-8]|9[1-9])[0-9]{3}[0-9]{4}$'), Validators.maxLength(17)],
        size: 6,
        order: 3
      }),
      new TextboxQuestion({
        key: 'street2',
        label: 'Address 1',
        value: this.type?.address?.street2,
        validators: [Validators.maxLength(35)],
        size: 6,
        order: 4
      }),
      new TextboxQuestion({
        key: 'fax',
        label: 'Fax',
        value: this.type?.address?.fax,
        mask: '(00) 0000-0000 || (00) 00000-0000',
        validators: [Validators.pattern('^[1-9]{2}(?:[2-8]|9[1-9])[0-9]{3}[0-9]{4}$'), Validators.maxLength(17)],
        size: 6,
        order: 5
      }),
      new TextboxQuestion({
        key: 'cityName',
        label: 'City',
        value: this.type?.address?.cityName,
        validators: [Validators.maxLength(35)],
        size: 6,
        order: 6
      }),
      new TextboxQuestion({
        key: 'website',
        label: 'Website',
        value: this.type?.address?.website,
        validators: [Validators.maxLength(255)],
        size: 6,
        order: 7
      }),
      new TextboxQuestion({
        key: 'postalCode',
        label: 'Zip Code',
        value: this.type?.address?.postalCode,
        mask: '00000-000',
        validators: [Validators.pattern('^[0-9]{2}[0-9]{3}[0-9]{3}$$'), Validators.maxLength(16)],
        size: 6,
        order: 8
      }),
      new TextboxQuestion({
        key: 'facebook',
        label: 'Facebook',
        value: this.type?.address?.facebook,
        validators: [Validators.maxLength(255)],
        size: 6,
        order: 9
      }),
      new TextboxQuestion({
        key: 'country',
        label: 'Country',
        value: this.type?.address?.country,
        validators: [Validators.maxLength(2)],
        size: 6,
        order: 10
      }),
      new TextboxQuestion({
        key: 'linkedin',
        label: 'Linkedin',
        value: this.type?.address?.linkedin,
        validators: [Validators.maxLength(255)],
        size: 6,
        order: 11
      }),
      new TextboxQuestion({
        key: 'stateOrProvince',
        label: 'State',
        value: this.type?.address?.stateOrProvince,
        validators: [Validators.maxLength(35)],
        size: 6,
        order: 12
      }),
      new TextboxQuestion({
        key: 'description',
        label: 'Note',
        value: this.type.description,
        validators: [Validators.maxLength(1024)],
        size: 12,
        order: 13
      }),
    ];
    this.navs = [{
      title: 'General Information',
      questions: this.customer
    },
    {
      title: 'Contact Information',
      questions: this.address
    }];
    let questions = [];
    this.navs.forEach(nav => questions = questions.concat(nav.questions));
    this.typeForm = this.questionControlService.toFormGroup(questions);
    this.ready = true;
  }

  public onSubmit(): boolean {
    if (this.typeForm.valid) {
      this.type.code = this.typeForm.controls.code.value;
      this.type.description = this.typeForm.controls.description.value;
      this.type.buyerEmail = this.typeForm.controls.buyerEmail.value;
      this.type.buyerMarketingEmail = this.typeForm.controls.buyerMarketingEmail.value;
      this.type.contactPhone = this.typeForm.controls.contactPhone.value;
      this.type.dateOfBirth = moment(this.typeForm.controls.dateOfBirth.value, 'DDMMYYYY').format();
      this.type.firstName = this.typeForm.controls.firstName.value;
      this.type.lastName = this.typeForm.controls.lastName.value;
      this.type.middleName = this.typeForm.controls.middleName.value;
      this.type.payer = this.typeForm.controls.payer.value;
      this.type.payerBusiness = this.typeForm.controls.payerBusiness.value;
      this.type.payerId = this.typeForm.controls.payerId.value;
      this.type.payerStatus = this.typeForm.controls.payerStatus.value;
      this.type.salutation = this.typeForm.controls.salutation.value;
      this.type.suffix = this.typeForm.controls.suffix.value;
      this.type.taxId = this.typeForm.controls.taxId.value;
      this.type.taxIdType = this.typeForm.controls.taxIdType.value;
      this.type.address.cityName = this.typeForm.controls.cityName.value;
      this.type.address.country = this.typeForm.controls.country.value;
      this.type.address.phone = this.typeForm.controls.phone.value;
      this.type.address.postalCode = this.typeForm.controls.postalCode.value;
      this.type.address.stateOrProvince = this.typeForm.controls.stateOrProvince.value;
      this.type.address.street1 = this.typeForm.controls.street1.value;
      this.type.address.street2 = this.typeForm.controls.street2.value;
      this.type.address.facebook = this.typeForm.controls.facebook.value;
      this.type.address.fax = this.typeForm.controls.fax.value;
      this.type.address.linkedin = this.typeForm.controls.linkedin.value;
      this.type.address.mobilePhone = this.typeForm.controls.mobilePhone.value;
      this.type.address.website = this.typeForm.controls.website.value;
      return true;
    }
    return false;
  }
}

@Component({
  selector: 'app-customer',
  templateUrl: '../type/abstract-type.component.html',
  styleUrls: ['./customer.component.sass']
})
export class CustomerComponent extends AbstractTypeComponent<Customer> {
  constructor(
    formBuilder: FormBuilder,
    modalService: NgbModal,
    typeService: TypeService<Customer>,
    typServiceTopic: TypeTopicService,
  ) {
    super(formBuilder, modalService, typeService, typServiceTopic, new Customer(), 'Customer Details');
  }

  public doGetColumns(): Column<Customer>[] {
    return [
      { key: 'name', title: 'Name', type: 'string' },
      { key: 'code', title: 'Code', type: 'string' },
      { key: 'description', title: 'Note', type: 'string' },
    ];
  }

  public doGetModal() {
    return CustomerModalComponent;
  }
}
