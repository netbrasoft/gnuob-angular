import { Address } from './address';
import { User } from '../authentication/user/user';
import { AbstractActiveType } from '../type/abstract-active-type';

export class Customer extends AbstractActiveType {
  user: User;
  address: Address;
  buyerEmail: string;
  buyerMarketingEmail: string;
  contactPhone: string;
  dateOfBirth: string;
  firstName: string;
  lastName: string;
  middleName: string;
  payer: string;
  payerBusiness: string;
  payerId: string;
  payerStatus: string;
  salutation: string;
  suffix: string;
  taxId: string;
  taxIdType: string;

  constructor(active?: boolean) {
    super('customer');
    super.active = active;
    this.address = new Address();
  }
}
