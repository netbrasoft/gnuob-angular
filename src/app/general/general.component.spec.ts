import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularDualListBoxModule } from 'angular-dual-listbox';
import { BlockUIModule } from 'ng-block-ui';
import { BlockUIHttpModule } from 'ng-block-ui/http';
import { GravatarModule } from 'ngx-gravatar';
import { environment } from 'src/environments/environment';
import { Message } from 'webstomp-client';
import { AppRoutingModule } from '../app-routing.module';
import { AuthenticationService } from '../authentication/authentication.service';
import { User } from '../authentication/user/user';
import { UserComponent, UserModalComponent } from '../authentication/user/user.component';
import { TypeComponentDirective } from '../type/directive/type-component.directive';
import { TypeTopicService } from '../type/type-topic.service';
import { GeneralComponent } from './general.component';

describe('GeneralComponent', () => {
  class WebSocketServiceSpy {
    public connect(path: string, callback?: (message: Message) => any): void { }

    public disconnect(disconnectCallback: () => any): void { }
  }
  let component: GeneralComponent;
  let fixture: ComponentFixture<GeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GeneralComponent,
        TypeComponentDirective,
        UserComponent,
        UserModalComponent,
      ],
      imports: [
        CommonModule,
        AngularDualListBoxModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserModule,
        GravatarModule,
        AppRoutingModule,
        HttpClientModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
        BlockUIModule.forRoot({ message: 'Please wait...' }),
        BlockUIHttpModule.forRoot({ blockAllRequestsInProgress: true }),
        NgbModule,
      ],
      providers: [
        AuthenticationService,
        { provide: TypeTopicService, useClass: WebSocketServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralComponent);
    component = fixture.componentInstance;
    component.user = new User();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
