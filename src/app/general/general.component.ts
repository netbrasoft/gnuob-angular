import { HttpErrorResponse } from '@angular/common/http';
import { Component, ComponentFactoryResolver, ComponentRef, OnDestroy, OnInit, Type, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import { AuthenticationService } from '../authentication/authentication.service';
import { PermissionComponent } from '../authentication/permission/permission.component';
import { RoleComponent } from '../authentication/role/role.component';
import { User } from '../authentication/user/user';
import { UserComponent } from '../authentication/user/user.component';
import { CategoryComponent } from '../category/category.component';
import { CustomerComponent } from '../customer/customer.component';
import { OfferComponent } from '../offer/offer.component';
import { AttributeComponent } from '../product/attribute/attribute.component';
import { ProductComponent } from '../product/product.component';
import { SettingComponent } from '../setting/setting.component';
import { TypeComponentDirective } from '../type/directive/type-component.directive';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-modal-confirm-logout',
  template: `
  <div class="modal-body border-left-primary">
    <button type="button" class="close" aria-label="Close button" aria-describedby="modal-title" (click)="modal.close('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
    <div class="text-center">
      <h1 class="h4 text-gray-900 mb-4">Ready to Leave?</h1>
    </div>
    <p>Select "Logout" below if you are ready to end your current session.</p>
  </div>
  <div class="modal-footer border-left-primary" style="border-top:0px">
    <button type="button" class="btn btn-outline-secondary" (click)="modal.close('Cancel click')">Cancel</button>
    <button type="button" ngbAutofocus class="btn btn-primary" (click)="modal.close('Logout click')">Logout</button>
  </div>
  `
})
export class ModalConfirmLogoutComponent {
  constructor(public modal: NgbActiveModal) { }
}

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.sass']
})
export class GeneralComponent implements OnInit, OnDestroy {
  @ViewChild(TypeComponentDirective, { static: true })
  private typeComponentDirective: TypeComponentDirective;
  private component: ComponentRef<Type<any>>;
  public user: User = JSON.parse(localStorage.getItem('user'));
  public alert: string = undefined;
  public name: string = environment.name;
  public version: string = environment.version;

  constructor(
    private authenticationService: AuthenticationService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private modalService: NgbModal,
    private router: Router,
  ) { }

  public ngOnInit(): void {
    this.show(UserComponent);
  }

  public onSidebarToggle(): void {
    $('body').toggleClass('sidebar-toggled');
    $('.sidebar').toggleClass('toggled');
  }

  public showUsers(): void {
    this.show(UserComponent);
  }

  public showRoles(): void {
    this.show(RoleComponent);
  }

  public showPermissions(): void {
    this.show(PermissionComponent);
  }

  public showSettings(): void {
    this.show(SettingComponent);
  }

  public showCategories(): void {
    this.show(CategoryComponent);
  }

  public showCustomers(): void {
    this.show(CustomerComponent);
  }

  public showProducts(): void {
    this.show(ProductComponent);
  }

  public showAttributes(): void {
    this.show(AttributeComponent);
  }

  public showOffers(): void {
    this.show(OfferComponent);
  }

  private show(componentRef: Type<any>): void {
    this.ngOnDestroy();
    this.typeComponentDirective.viewContainerRef.clear();
    this.component = this.typeComponentDirective.viewContainerRef
      .createComponent(this.componentFactoryResolver.resolveComponentFactory(componentRef));
  }

  public ngOnDestroy(): void {
    if (!!this.component) {
      this.component.destroy();
    }
  }

  public async confirmLogout(): Promise<void> {
    const ngbModalRef: NgbModalRef = this.modalService.open(ModalConfirmLogoutComponent);
    ngbModalRef.result.then(result => {
      if (result === 'Logout click') {
        this.logout();
      }
    });
  }

  private async logout(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.authenticationService.logout(this.user)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then(() => {
      localStorage.clear();
      this.router.navigate(['/login']);
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}
