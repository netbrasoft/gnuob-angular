import { TestBed } from '@angular/core/testing';
import { GeneralGuard } from './general.guard';
import { AppRoutingModule } from '../app-routing.module';

describe('GeneralGuard', () => {
  let guard: GeneralGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppRoutingModule,
      ]
    });
    guard = TestBed.inject(GeneralGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
