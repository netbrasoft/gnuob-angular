import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from '../../app-routing.module';
import { Message } from 'webstomp-client';
import { LocalTypeService } from '../../type/local-type.service';
import { OfferRecordComponent } from './offer-record.component';
import { TypeTopicService } from '../../type/type-topic.service';

describe('OfferRecordComponent', () => {
  class WebSocketServiceSpy {
    public connect(path: string, callback?: (message: Message) => any): void { }

    public disconnect(disconnectCallback: () => any): void { }
  }
  let component: OfferRecordComponent;
  let fixture: ComponentFixture<OfferRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OfferRecordComponent],
      imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
      ],
      providers: [
        { provide: TypeTopicService, useClass: WebSocketServiceSpy }
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfferRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
