import { Component, Type } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Question } from 'src/app/type/question/question';
import { SelectQuestion } from 'src/app/type/question/select-question';
import { TextboxQuestion } from 'src/app/type/question/textbox-question';
import { TreeviewQuestion } from 'src/app/type/question/treeview-question';
import { getAttributeTreeviewItems } from 'src/app/utils';
import { AbstractTypeComponent, AbstractTypeModalComponent, Modal } from '../../type/abstract-type.component';
import { Column } from '../../type/directive/sortable-header.directive';
import { LocalTypeService } from '../../type/local-type.service';
import { QuestionControlService } from '../../type/question/question-control.service';
import { TypeTopicService } from '../../type/type-topic.service';
import { Record } from '../record';

@Component({
  selector: 'app-offer-record-modal',
  templateUrl: '../../type/abstract-type-modal.component.html'
})
export class OfferRecordModalComponent<T extends Record> extends AbstractTypeModalComponent<T> {
  public record: Question<any>[] = [];

  constructor(
    private questionControlService: QuestionControlService,
  ) {
    super();
  }

  public async onInit(): Promise<void> {
    this.record = [
      new TextboxQuestion({
        key: 'code',
        label: 'SKU',
        value: { value: this.type.code, disabled: true },
        validators: [Validators.required, Validators.maxLength(255)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'name',
        label: 'Name',
        value: { value: this.type.name, disabled: true },
        validators: [Validators.required, Validators.maxLength(125)],
        size: 6,
        order: 2
      }),
      new TextboxQuestion({
        key: 'amount',
        label: 'Price',
        mask: 'separator.2',
        value: { value: this.type.amount, disabled: true },
        size: 3,
        order: 3
      }),
      new TextboxQuestion({
        key: 'discount',
        label: 'Discount',
        mask: 'separator.2',
        value: { value: this.type.discount, disabled: true },
        size: 3,
        order: 4
      }),
      new TextboxQuestion({
        key: 'shippingCost',
        label: 'Shipping Cost',
        mask: 'separator.2',
        value: { value: this.type.shippingCost, disabled: true },
        size: 3,
        order: 5
      }),
      new TextboxQuestion({
        key: 'tax',
        label: 'Tax',
        mask: 'separator.2',
        value: { value: this.type.tax, disabled: true },
        size: 3,
        order: 6
      }),
      new TextboxQuestion({
        key: 'quantity',
        label: 'Quantity',
        mask: 'separator.2',
        value: { value: this.type.quantity, disabled: true },
        size: 4,
        order: 7
      }),
      new Question({
        size: 8,
        order: 8
      }),
      new TextboxQuestion({
        key: 'itemWeight',
        label: 'Weight',
        mask: '000',
        value: { value: this.type.itemWeight, disabled: true },
        size: 3,
        order: 9
      }),
      new SelectQuestion({
        key: 'itemWeightUnit',
        label: 'Weight Unit',
        value: { value: this.type.itemWeightUnit, disabled: true },
        options: [
          { value: 'Kg', label: 'Kilogram' },
          { value: 'g', label: 'Gram' },
        ],
        size: 3,
        order: 10
      }),
      new TextboxQuestion({
        key: 'itemLength',
        label: 'Length',
        mask: '000',
        value: { value: this.type.itemLength, disabled: true },
        size: 3,
        order: 11
      }),
      new SelectQuestion({
        key: 'itemLengthUnit',
        label: 'Length Unit',
        value: { value: this.type.itemLengthUnit, disabled: true },
        options: [
          { value: 'M', label: 'Meter' },
          { value: 'Cm', label: 'Centimeter' },
        ],
        size: 3,
        order: 12
      }),
      new TextboxQuestion({
        key: 'itemWidth',
        label: 'Width',
        mask: '000',
        value: { value: this.type.itemWidth, disabled: true },
        size: 3,
        order: 12
      }),
      new SelectQuestion({
        key: 'itemWidthUnit',
        label: 'Width Unit',
        value: { value: this.type.itemWidthUnit, disabled: true },
        size: 3,
        options: [
          { value: 'M', label: 'Meter' },
          { value: 'Cm', label: 'Centimeter' },
        ],
        order: 13
      }),
      new TextboxQuestion({
        key: 'itemHeight',
        label: 'Height',
        mask: '000',
        value: { value: this.type.itemHeight, disabled: true },
        size: 3,
        order: 15
      }),
      new SelectQuestion({
        key: 'itemHeightUnit',
        label: 'Height Unit',
        value: { value: this.type.itemHeightUnit, disabled: true },
        size: 3,
        options: [
          { value: 'M', label: 'Meter' },
          { value: 'Cm', label: 'Centimeter' },
        ],
        order: 14
      }),
      new TextboxQuestion({
        key: 'itemUrl',
        label: 'Url',
        value: { value: this.type.itemUrl, disabled: true },
        size: 9,
        order: 15
      }),
      new TreeviewQuestion({
        key: 'attributes',
        label: 'Attributes',
        value: this.type.attributes,
        source: getAttributeTreeviewItems(this.type.attributes, this.type.attributes),
        options: {
          hasAllCheckBox: true,
          hasFilter: true,
          hasCollapseExpand: true,
          decoupleChildFromParent: false,
          maxHeight: 500
        },
        size: 3,
        order: 16
      }),
      new TextboxQuestion({
        key: 'description',
        label: 'Note',
        value: { value: this.type.description, disabled: true },
        validators: [Validators.maxLength(1024)],
        order: 17
      }),
    ];
    this.navs = [{
      title: 'General Information',
      questions: this.record
    }];
    let questions = [];
    this.navs.forEach(nav => questions = questions.concat(nav.questions));
    this.typeForm = this.questionControlService.toFormGroup(questions);
    this.ready = true;
  }

  public onSubmit(): boolean {
    return false;
  }
}

@Component({
  selector: 'app-offer-record',
  templateUrl: '../../type/abstract-type.component.html',
  styleUrls: ['./offer-record.component.sass']
})
export class OfferRecordComponent extends AbstractTypeComponent<Record> {
  constructor(
    formBuilder: FormBuilder,
    modalService: NgbModal,
    typeService: LocalTypeService<Record>,
    typServiceTopic: TypeTopicService,
  ) {
    super(formBuilder, modalService, typeService, typServiceTopic, new Record(), 'Offer Record Details');
    //this.addButton = false;
    this.deleteButton = false;
    this.saveButton = false;
    this.statusButton = false;
    this.showTitle = false;
  }

  public doGetColumns(): Column<Record>[] {
    return [
      { key: 'name', title: 'Name', type: 'string' },
      { key: 'code', title: 'Code', type: 'string' },
      { key: 'description', title: 'Note', type: 'string' },
    ];
  }

  public doGetModal(): Type<Modal<Record>> {
    return OfferRecordModalComponent;
  }
}
