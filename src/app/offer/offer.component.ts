import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Offer } from '../offer/offer';
import { AbstractTypeComponent, AbstractTypeModalComponent } from '../type/abstract-type.component';
import { Column } from '../type/directive/sortable-header.directive';
import { LocalTypeService } from '../type/local-type.service';
import { Question } from '../type/question/question';
import { QuestionControlService } from '../type/question/question-control.service';
import { TemplateQuestion } from '../type/question/template-question';
import { TextboxQuestion } from '../type/question/textbox-question';
import { TypeTopicService } from '../type/type-topic.service';
import { TypeService } from '../type/type.service';
import { OfferRecordComponent } from './offer-record/offer-record.component';
import { Record } from './record';

@Component({
  selector: 'app-offer-modal',
  templateUrl: '../type/abstract-type-modal.component.html'
})
export class OfferModalComponent<T extends Offer> extends AbstractTypeModalComponent<T> {
  public offer: Question<any>[] = [];
  public offerRecord: Question<any>[] = [];

  constructor(
    private offerRecordService: LocalTypeService<Record>,
    private questionControlService: QuestionControlService,
  ) {
    super();
  }

  public async onInit(): Promise<void> {
    await this.saveAllOfferRecords();
    this.offer = [
      new TextboxQuestion({
        key: 'code',
        label: 'Reference',
        value: { value: this.type.code, disabled: true },
        validators: [Validators.required, Validators.maxLength(255)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'name',
        label: 'Customer',
        value: { value: this.type?.customer?.name, disabled: true },
        validators: [Validators.required, Validators.maxLength(25)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'discountTotal',
        label: 'Discount Total',
        mask: 'separator.2',
        value: { value: this.type.discountTotal, disabled: true },
        size: 3,
        order: 3
      }),
      new TextboxQuestion({
        key: 'extraAmount',
        label: 'Extra Amount',
        mask: 'separator.2',
        value: { value: this.type.extraAmount, disabled: true },
        size: 3,
        order: 4
      }),
      new TextboxQuestion({
        key: 'handlingTotal',
        label: 'Handling Total',
        mask: 'separator.2',
        value: { value: this.type.handlingTotal, disabled: true },
        size: 3,
        order: 5
      }),
      new TextboxQuestion({
        key: 'insuranceTotal',
        label: 'Insurance Total',
        mask: 'separator.2',
        value: { value: this.type.insuranceTotal, disabled: true },
        size: 3,
        order: 6
      }),
      new TextboxQuestion({
        key: 'itemTotal',
        label: 'Item Total',
        mask: 'separator.2',
        value: { value: this.type.itemTotal, disabled: true },
        size: 3,
        order: 7
      }),
      new TextboxQuestion({
        key: 'maxTotal',
        label: 'Max Total',
        mask: 'separator.2',
        value: { value: this.type.maxTotal, disabled: true },
        size: 3,
        order: 8
      }),
      new TextboxQuestion({
        key: 'shippingDiscount',
        label: 'Shipping Discount',
        mask: 'separator.2',
        value: { value: this.type.shippingDiscount, disabled: true },
        size: 3,
        order: 9
      }),
      new TextboxQuestion({
        key: 'shippingTotal',
        label: 'Shipping Total',
        mask: 'separator.2',
        value: { value: this.type.shippingTotal, disabled: true },
        size: 3,
        order: 10
      }),
      new TextboxQuestion({
        key: 'taxTotal',
        label: 'Tax Total',
        mask: 'separator.2',
        value: { value: this.type.taxTotal, disabled: true },
        size: 3,
        order: 11
      }),
      new TextboxQuestion({
        key: 'total',
        label: 'Total',
        mask: 'separator.2',
        value: { value: this.type.total, disabled: true },
        size: 3,
        order: 12
      }),
      new Question({
        size: 6,
        order: 13
      }),
      new TextboxQuestion({
        key: 'description',
        label: 'Note',
        value: { value: this.type.description, disabled: true },
        validators: [Validators.maxLength(1024)],
        order: 14
      }),
    ];
    this.offerRecord = [
      new TemplateQuestion({
        key: 'records',
        value: this.type.records,
        componentRef: OfferRecordComponent,
        size: 12,
        order: 1
      })
    ];
    this.navs = [{
      title: 'General Information',
      questions: this.offer
    }, {
      title: 'Offer Records',
      questions: this.offerRecord
    }];
    let questions = [];
    this.navs.forEach(nav => questions = questions.concat(nav.questions));
    this.typeForm = this.questionControlService.toFormGroup(questions);
    this.ready = true;
  }

  public onSubmit(): boolean {
    return false;
  }

  private async saveAllOfferRecords(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.type.records.push(new Record());
      this.offerRecordService.saveAll(this.type.records)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then((records) => {
      this.type.records = records;
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}

@Component({
  selector: 'app-offer',
  templateUrl: '../type/abstract-type.component.html',
  styleUrls: ['./offer.component.sass']
})
export class OfferComponent extends AbstractTypeComponent<Offer> {
  constructor(
    formBuilder: FormBuilder,
    modalService: NgbModal,
    typeService: TypeService<Offer>,
    typServiceTopic: TypeTopicService,
  ) {
    super(formBuilder, modalService, typeService, typServiceTopic, new Offer(), 'Offer Details');
    //this.addButton = false;
    this.deleteButton = false;
    this.saveButton = false;
    this.statusButton = false;
  }

  public doGetColumns(): Column<Offer>[] {
    return [
      { key: 'name', title: 'Name', type: 'string' },
      { key: 'code', title: 'Code', type: 'string' },
      { key: 'description', title: 'Note', type: 'string' },
    ];
  }

  public doGetModal() {
    return OfferModalComponent;
  }
}
