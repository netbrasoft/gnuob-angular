import { Customer } from '../customer/customer';
import { AbstractActiveType } from '../type/abstract-active-type';
import { Record } from './record';

export class Offer extends AbstractActiveType {
  discountTotal: number;
  extraAmount: number;
  handlingTotal: number;
  insuranceTotal: number;
  itemTotal: number;
  maxTotal: number;
  shippingDiscount: number;
  shippingTotal: number;
  taxTotal: number;
  total: number;
  customer: Customer;
  records: Record[] = [];

  constructor(active?: boolean) {
    super('offer');
    super.active = active;
  }
}
