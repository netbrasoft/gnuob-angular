import { Attribute } from '../product/attribute/attribute';
import { Product } from '../product/product';
import { AbstractActiveType } from '../type/abstract-active-type';

export class Record extends AbstractActiveType {
  amount: number;
  discount: number;
  itemHeight: number;
  itemLength: number;
  itemWeight: number;
  itemWidth: number;
  shippingCost: number;
  tax: number;
  quantity: number;
  position: number;
  itemHeightUnit: string;
  itemLengthUnit: string;
  itemUrl: string;
  itemWeightUnit: string;
  itemWidthUnit: string;
  product: Product;
  attributes: Attribute[] = [];

  constructor(active?: boolean) {
    super('record');
    super.active = active;
    this.product = new Product();
  }
}
