import { AbstractActiveType } from 'src/app/type/abstract-active-type';

export class Attribute extends AbstractActiveType {
  position: number;
  parent: Attribute;

  constructor(active?: boolean) {
    super('attribute');
    super.active = active;
  }
}