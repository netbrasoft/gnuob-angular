import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { Category } from '../category/category';
import { AbstractTypeComponent, AbstractTypeModalComponent } from '../type/abstract-type.component';
import { Column } from '../type/directive/sortable-header.directive';
import { CheckboxQuestion } from '../type/question/checkbox-question';
import { Question } from '../type/question/question';
import { QuestionControlService } from '../type/question/question-control.service';
import { RatingQuestion } from '../type/question/rating-question';
import { SelectQuestion } from '../type/question/select-question';
import { TextboxQuestion } from '../type/question/textbox-question';
import { TreeviewQuestion } from '../type/question/treeview-question';
import { WysiwygQuestion } from '../type/question/wysiwyg-question';
import { TypeTopicService } from '../type/type-topic.service';
import { TypeService } from '../type/type.service';
import { getAttributeTreeviewItems, getCategoryTreeviewItems } from '../utils';
import { Attribute } from './attribute/attribute';
import { Product } from './product';

@Component({
  selector: 'app-product-modal',
  templateUrl: '../type/abstract-type-modal.component.html'
})
export class ProductModalComponent<T extends Product> extends AbstractTypeModalComponent<T> {
  private categories: Category[] = [];
  private attributes: Attribute[] = [];
  public product: Question<any>[] = [];
  public content: Question<any>[] = [];

  constructor(
    private attributeService: TypeService<Attribute>,
    private categoryService: TypeService<Category>,
    private questionControlService: QuestionControlService,
  ) {
    super();
    moment.updateLocale(moment.locale(), { invalidDate: '' });
  }

  public async onInit(): Promise<void> {
    await this.findAllCategories();
    await this.findAllAttribute();
    this.product = [
      new TextboxQuestion({
        key: 'code',
        label: 'SKU',
        value: this.type.code,
        required: true,
        validators: [Validators.required, Validators.maxLength(255)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'name',
        label: 'Name',
        value: this.type.name,
        required: true,
        validators: [Validators.required, Validators.maxLength(125)],
        size: 6,
        order: 2
      }),
      new TextboxQuestion({
        key: 'amount',
        label: 'Price',
        mask: 'separator.2',
        value: this.type.amount,
        size: 3,
        order: 3
      }),
      new TextboxQuestion({
        key: 'discount',
        label: 'Discount',
        mask: 'separator.2',
        value: this.type.discount,
        size: 3,
        order: 4
      }),
      new TextboxQuestion({
        key: 'shippingCost',
        label: 'Shipping Cost',
        mask: 'separator.2',
        value: this.type.shippingCost,
        size: 3,
        order: 5
      }),
      new TextboxQuestion({
        key: 'tax',
        label: 'Tax',
        mask: 'separator.2',
        value: this.type.tax,
        size: 3,
        order: 6
      }),
      new TextboxQuestion({
        key: 'quantity',
        label: 'Quantity',
        mask: '00000',
        value: this.type?.stock?.quantity,
        size: 4,
        order: 7
      }),
      new TextboxQuestion({
        key: 'minQuantity',
        label: 'Min Quantity',
        mask: '00000',
        value: this.type?.stock?.minQuantity,
        size: 4,
        order: 8
      }),
      new TextboxQuestion({
        key: 'maxQuantity',
        label: 'Max Quantity',
        mask: '00000',
        value: this.type?.stock?.maxQuantity,
        size: 4,
        order: 9
      }),
      new TextboxQuestion({
        key: 'itemWeight',
        label: 'Weight',
        mask: '000',
        value: this.type.itemWeight,
        size: 3,
        order: 10
      }),
      new SelectQuestion({
        key: 'itemWeightUnit',
        label: 'Weight Unit',
        value: this.type.itemWeightUnit,
        options: [
          { value: 'Kg', label: 'Kilogram' },
          { value: 'g', label: 'Gram' },
        ],
        size: 3,
        order: 11
      }),
      new TextboxQuestion({
        key: 'itemLength',
        label: 'Length',
        mask: '000',
        value: this.type.itemLength,
        size: 3,
        order: 12
      }),
      new SelectQuestion({
        key: 'itemLengthUnit',
        label: 'Length Unit',
        value: this.type.itemLengthUnit,
        options: [
          { value: 'M', label: 'Meter' },
          { value: 'Cm', label: 'Centimeter' },
        ],
        size: 3,
        order: 13
      }),
      new TextboxQuestion({
        key: 'itemWidth',
        label: 'Width',
        mask: '000',
        value: this.type.itemWidth,
        size: 3,
        order: 14
      }),
      new SelectQuestion({
        key: 'itemWidthUnit',
        label: 'Width Unit',
        value: this.type.itemWidthUnit,
        size: 3,
        options: [
          { value: 'M', label: 'Meter' },
          { value: 'Cm', label: 'Centimeter' },
        ],
        order: 15
      }),
      new TextboxQuestion({
        key: 'itemHeight',
        label: 'Height',
        mask: '000',
        value: this.type.itemHeight,
        size: 3,
        order: 16
      }),
      new SelectQuestion({
        key: 'itemHeightUnit',
        label: 'Height Unit',
        value: this.type.itemHeightUnit,
        size: 3,
        options: [
          { value: 'M', label: 'Meter' },
          { value: 'Cm', label: 'Centimeter' },
        ],
        order: 17
      }),
      new TextboxQuestion({
        key: 'description',
        label: 'Note',
        value: this.type.description,
        validators: [Validators.maxLength(1024)],
        order: 18
      }),
    ];
    this.content = [
      new TextboxQuestion({
        key: 'itemUrl',
        label: 'Url',
        value: this.type.itemUrl,
        size: 6,
        order: 1
      }),
      new TreeviewQuestion({
        key: 'attributes',
        label: 'Attributes',
        value: this.type.attributes,
        source: getAttributeTreeviewItems(this.attributes, this.type.attributes),
        options: {
          hasAllCheckBox: true,
          hasFilter: true,
          hasCollapseExpand: true,
          decoupleChildFromParent: false,
          maxHeight: 500
        },
        size: 3,
        order: 2
      }),
      new TreeviewQuestion({
        key: 'categories',
        label: 'Categories',
        value: this.type.categories,
        source: getCategoryTreeviewItems(this.categories, this.type.categories),
        options: {
          hasAllCheckBox: true,
          hasFilter: true,
          hasCollapseExpand: true,
          decoupleChildFromParent: false,
          maxHeight: 500
        },
        size: 3,
        order: 3
      }),
      new WysiwygQuestion({
        key: 'content',
        label: 'Content',
        value: this.type.content,
        required: true,
        validators: [Validators.required],
        size: 12,
        order: 1,
        options: {
          height: 500,
          menubar: true,
          plugins: [
            'advlist autolink lists link image charmap print',
            'preview anchor searchreplace visualblocks code',
            'fullscreen insertdatetime media table paste',
            'help wordcount'
          ],
          toolbar:
            'undo redo | formatselect | bold italic | \
            alignleft aligncenter alignright alignjustify | \
            bullist numlist outdent indent | help'
        }
      }),
      new CheckboxQuestion({
        key: 'bestsellers',
        label: 'Bestsellers',
        value: this.type.bestsellers,
        size: 3,
        order: 4
      }),
      new CheckboxQuestion({
        key: 'latestCollection',
        label: 'Latest Collection',
        value: this.type.latestCollection,
        size: 3,
        order: 5
      }),
      new CheckboxQuestion({
        key: 'recommended',
        label: 'Recommended',
        value: this.type.recommended,
        size: 3,
        order: 6
      }),
      new RatingQuestion({
        key: 'rating',
        label: 'Rate',
        value: this.type.rating,
        size: 3,
        order: 7
      }),
    ];
    this.navs = [{
      title: 'General Information',
      questions: this.product
    }, {
      title: 'Product Content',
      questions: this.content
    }];
    let questions = [];
    this.navs.forEach(nav => questions = questions.concat(nav.questions));
    this.typeForm = this.questionControlService.toFormGroup(questions);
    this.ready = true;
  }

  public onSubmit(): boolean {
    if (this.typeForm.valid) {
      this.type.code = this.typeForm.controls.code.value;
      this.type.name = this.typeForm.controls.name.value;
      this.type.description = this.typeForm.controls.description.value;
      this.type.amount = this.typeForm.controls.amount.value;
      this.type.discount = this.typeForm.controls.discount.value;
      this.type.itemHeight = this.typeForm.controls.itemHeight.value;
      this.type.itemLength = this.typeForm.controls.itemLength.value;
      this.type.itemWeight = this.typeForm.controls.itemWeight.value;
      this.type.itemWidth = this.typeForm.controls.itemWidth.value;
      this.type.shippingCost = this.typeForm.controls.shippingCost.value;
      this.type.tax = this.typeForm.controls.tax.value;
      this.type.bestsellers = this.typeForm.controls.bestsellers.value;
      this.type.latestCollection = this.typeForm.controls.latestCollection.value;
      this.type.recommended = this.typeForm.controls.recommended.value;
      this.type.rating = this.typeForm.controls.rating.value;
      this.type.content = this.typeForm.controls.content.value;
      this.type.itemHeightUnit = this.typeForm.controls.itemHeightUnit.value;
      this.type.itemLengthUnit = this.typeForm.controls.itemLengthUnit.value;
      this.type.itemUrl = this.typeForm.controls.itemUrl.value;
      this.type.itemWeightUnit = this.typeForm.controls.itemWeightUnit.value;
      this.type.itemWidthUnit = this.typeForm.controls.itemWidthUnit.value;
      this.type.stock.quantity = this.typeForm.controls.quantity.value;
      this.type.stock.minQuantity = this.typeForm.controls.minQuantity.value;
      this.type.stock.maxQuantity = this.typeForm.controls.maxQuantity.value;
      this.type.attributes = this.typeForm.controls.attributes.value;
      this.type.categories = this.typeForm.controls.categories.value;
      return true;
    }
    return false;
  }

  private async findAllCategories(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.categoryService.findAll(1, 100, 'asc', ['position'], new Category(true))
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: Category[] }) => {
      this.categories = data.types;
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }

  private async findAllAttribute(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.attributeService.findAll(1, 100, 'asc', ['position'], new Attribute(true))
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: Attribute[] }) => {
      this.attributes = data.types;
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}

@Component({
  selector: 'app-product',
  templateUrl: '../type/abstract-type.component.html',
  styleUrls: ['./product.component.sass']
})
export class ProductComponent extends AbstractTypeComponent<Product> {
  constructor(
    formBuilder: FormBuilder,
    modalService: NgbModal,
    typeService: TypeService<Product>,
    typServiceTopic: TypeTopicService,
  ) {
    super(formBuilder, modalService, typeService, typServiceTopic, new Product(), 'Product Details');
  }

  public doGetColumns(): Column<Product>[] {
    return [
      { key: 'name', title: 'Name', type: 'string' },
      { key: 'code', title: 'Code', type: 'string' },
      { key: 'description', title: 'Note', type: 'string' },
    ];
  }

  public doGetModal() {
    return ProductModalComponent;
  }
}