import { Category } from '../category/category';
import { AbstractActiveType } from '../type/abstract-active-type';
import { Attribute } from './attribute/attribute';
import { Stock } from './stock';

export class Product extends AbstractActiveType {
  amount: number;
  discount: number;
  itemHeight: number;
  itemLength: number;
  itemWeight: number;
  itemWidth: number;
  shippingCost: number;
  tax: number;
  bestsellers: boolean;
  latestCollection: boolean;
  recommended: boolean;
  rating: number;
  stock: Stock;
  content: string;
  itemHeightUnit: string;
  itemLengthUnit: string;
  itemUrl: string;
  itemWeightUnit: string;
  itemWidthUnit: string;
  categories: Category[] = [];
  attributes: Attribute[] = [];

  constructor(active?: boolean) {
    super('product');
    super.active = active;
    this.stock = new Stock();
  }
}
