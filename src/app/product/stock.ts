import { AbstractType } from '../type/abstract-type';

export class Stock extends AbstractType {
  maxQuantity: number;
  minQuantity: number;
  quantity: number;

  constructor() {
    super('stock');
  }
}
