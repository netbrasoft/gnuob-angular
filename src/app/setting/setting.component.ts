import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractTypeComponent, AbstractTypeModalComponent } from '../type/abstract-type.component';
import { Column } from '../type/directive/sortable-header.directive';
import { Question } from '../type/question/question';
import { QuestionControlService } from '../type/question/question-control.service';
import { SelectQuestion } from '../type/question/select-question';
import { TextboxQuestion } from '../type/question/textbox-question';
import { TypeTopicService } from '../type/type-topic.service';
import { TypeService } from '../type/type.service';
import { Setting, Type } from './setting';

@Component({
  selector: 'app-setting-modal',
  templateUrl: '../type/abstract-type-modal.component.html'
})
export class SettingModalComponent<T extends Setting> extends AbstractTypeModalComponent<T> {
  public setting: Question<string>[] = [];

  constructor(
    private questionControlService: QuestionControlService,
  ) {
    super();
  }

  public async onInit(): Promise<void> {
    this.setting = [
      new TextboxQuestion({
        key: 'name',
        label: 'Name',
        value: this.type.name,
        required: true,
        validators: [Validators.required, Validators.maxLength(125)],
        size: 6,
        order: 1
      }),
      new TextboxQuestion({
        key: 'code',
        label: 'Code',
        value: this.type.code,
        required: true,
        validators: [Validators.required, Validators.maxLength(255)],
        size: 6,
        order: 2
      }),
      new TextboxQuestion({
        key: 'description',
        label: 'Note',
        value: this.type.description,
        validators: [Validators.maxLength(1024)],
        order: 3
      }),
      new TextboxQuestion({
        key: 'value',
        label: 'Value',
        value: this.type.value,
        required: true,
        validators: [Validators.required, Validators.maxLength(255)],
        size: 6,
        order: 5
      }),
      new SelectQuestion({
        key: 'type',
        label: 'Type',
        value: this.type.type,
        required: true,
        validators: [Validators.required],
        size: 6,
        options: [
          { value: Type.BOOLEAN, label: 'BOOLEAN' },
          { value: Type.DATE, label: 'DATE' },
          { value: Type.DOUBLE, label: 'DOUBLE' },
          { value: Type.EMAIL, label: 'EMAIL' },
          { value: Type.MENU, label: 'MENU' },
          { value: Type.STRING, label: 'STRING' },
          { value: Type.URL, label: 'URL' },
        ],
        order: 4
      }),
    ];
    this.navs = [{
      title: 'General Information',
      questions: this.setting
    }];
    let questions = [];
    this.navs.forEach(nav => questions = questions.concat(nav.questions));
    this.typeForm = this.questionControlService.toFormGroup(questions);
    this.ready = true;
  }

  public onSubmit(): boolean {
    if (this.typeForm.valid) {
      this.type.name = this.typeForm.controls.name.value;
      this.type.code = this.typeForm.controls.code.value;
      this.type.description = this.typeForm.controls.description.value;
      this.type.type = this.typeForm.controls.type.value.value;
      this.type.value = this.typeForm.controls.value.value;
      return true;
    }
    return false;
  }
}

@Component({
  selector: 'app-setting',
  templateUrl: '../type/abstract-type.component.html',
  styleUrls: ['./setting.component.sass']
})
export class SettingComponent extends AbstractTypeComponent<Setting> {
  constructor(
    formBuilder: FormBuilder,
    modalService: NgbModal,
    typeService: TypeService<Setting>,
    typServiceTopic: TypeTopicService,
  ) {
    super(formBuilder, modalService, typeService, typServiceTopic, new Setting(), 'Setting Details');
  }

  public doGetColumns(): Column<Setting>[] {
    return [
      { key: 'name', title: 'Name', type: 'string' },
      { key: 'code', title: 'Code', type: 'string' },
      { key: 'description', title: 'Note', type: 'string' },
    ];
  }

  public doGetModal() {
    return SettingModalComponent;
  }
}
