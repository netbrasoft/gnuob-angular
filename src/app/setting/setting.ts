import { AbstractActiveType } from '../type/abstract-active-type';

export enum Type {
  BOOLEAN = 'BOOLEAN', DATE = 'DATE', DOUBLE = 'DOUBLE', EMAIL = 'EMAIL', INTEGER = 'INTEGER', MENU = 'MENU', STRING = 'STRING', URL = 'URL'
}

export class Setting extends AbstractActiveType {
  value: string;
  type: string;

  constructor(active?: boolean) {
    super('setting');
    super.active = active;
  }
}
