import { AbstractType } from './abstract-type';

export abstract class AbstractActiveType extends AbstractType {
  checked = false;
  code: string;
  name: string;
  description: string;
  active: boolean;
}
