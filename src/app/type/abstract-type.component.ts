import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, ComponentFactoryResolver, ComponentRef, Directive, OnDestroy, OnInit, QueryList, Type, ViewChild, ViewChildren } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { BehaviorSubject, Observable } from 'rxjs';
import { Message } from 'webstomp-client';
import { deepClone, getObjectKey } from '../utils';
import { AbstractActiveType } from './abstract-active-type';
import { AbstractType } from './abstract-type';
import { Column, SortableHeaderDirective, SortEvent, State } from './directive/sortable-header.directive';
import { TypeModalComponentDirective } from './directive/type-modal-component.directive';
import { Question } from './question/question';
import { TypeService } from './type-service';
import { TypeTopic } from './type-topic';
import { TypeTopicService } from './type-topic-service';

export interface Modal<T extends AbstractType> {
  type: T;
  typeForm: FormGroup;
  ready: boolean;
  alert: string;
  onInit(): void;
  onSubmit(): boolean;
}

export interface QuestionNavModal<T extends AbstractType> extends Modal<T> {
  navs: QuestionNav[];
}

export interface QuestionNav {
  title: string;
  questions: Question<string>[];
}

export abstract class AbstractTypeModalComponent<T extends AbstractType> implements QuestionNavModal<T> {
  public type: T;
  public typeForm: FormGroup;
  public navs: QuestionNav[] = [];
  public ready = false;
  public alert: string = undefined;

  constructor() { }

  trackByFn(index: any, item: any) {
    return index;
  }

  public abstract onSubmit(): boolean;

  public abstract async onInit(): Promise<void>;
}

@Component({
  selector: 'app-modal',
  template: `
    <div class="modal-body border-left-primary">
      <button type="button" class="close" aria-label="Close" (click)="activeModal.close('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="row">
        <div class="col-lg-12">
          <div class="p-5">
            <div class="text-center">
              <h1 class="h4 text-gray-900 mb-4">{{title}}</h1>
            </div>
            <ng-template appTypeModalComponent></ng-template>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer border-left-primary" style="border-top:0px">
      <button type="button" class="btn btn-outline-secondary" (click)="onCancel()">Cancel</button>
      <button *ngIf="saveButton" type="button" ngbAutofocus class="btn btn-primary" [disabled]="invalid()" (click)="onSubmit()">Save</button>
    </div>
    `
})
export class ModalComponent<T extends AbstractActiveType> implements OnInit, OnDestroy {
  @ViewChild(TypeModalComponentDirective, { static: true })
  private typeModalComponentDirective: TypeModalComponentDirective;
  private modal: ComponentRef<Modal<T>>;
  public type: T;
  public title: string = undefined;
  public saveButton = true;
  public componentRef: Type<Modal<T>>;
  public typeService: TypeService<T>;

  constructor(
    public componentFactoryResolver: ComponentFactoryResolver,
    public activeModal: NgbActiveModal,
  ) { }

  public ngOnInit(): void {
    this.typeModalComponentDirective.viewContainerRef.clear();
    this.modal = this.typeModalComponentDirective.viewContainerRef
      .createComponent(this.componentFactoryResolver.resolveComponentFactory(this.componentRef));
    this.modal.instance.type = this.type;
    this.modal.instance.onInit();
  }

  ngOnDestroy(): void {
    this.modal.destroy();
  }

  public async onSubmit(): Promise<void> {
    if (this.modal.instance.onSubmit()) {
      await this.save();
    }
  }

  public onCancel(): void {
    this.activeModal.close('Cancel click');
  }

  public invalid(): boolean {
    return !this.modal?.instance?.typeForm ? true : this.modal.instance.typeForm?.invalid;
  }

  public async save(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.typeService.save(this.type)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then(async () => {
      this.activeModal.close('Save click');
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.modal.instance.alert = error.error.message;
      } else {
        this.modal.instance.alert = error?.message;
      }
    });
  }
}

@Component({
  selector: 'app-type-confirm-delete-modal',
  template: `
    <div class="modal-body border-left-danger">
      <button type="button" class="close" aria-label="Close button" aria-describedby="modal-title" (click)="modal.close('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
      <div class="text-center">
        <h1 class="h4 text-gray-900 mb-4">{{title}} Deletion!</h1>
      </div>
      <p><strong>Are you sure you want to delete <span class="text-primary">"{{type?.name}}{{!!types?.length ? 'the ' + types?.length + ' selected item(s)' : ''}}"</span>?</strong></p>
      <p>All information associated to this item will be permanently deleted.
      <span class="text-danger">This operation can not be undone.</span>
      </p>
    </div>
    <div class="modal-footer border-left-danger" style="border-top:0px">
      <button type="button" class="btn btn-outline-secondary" (click)="modal.close('Cancel click')">Cancel</button>
      <button type="submit" ngbAutofocus class="btn btn-danger" (click)="modal.close('Ok click')">Ok</button>
    </div>
    `
})
export class TypeConfirmDeleteModalComponent<T extends AbstractActiveType> {
  public type: T;
  public types: T[];
  public title: string = undefined;

  constructor(public modal: NgbActiveModal) { }
}
@Directive()
export abstract class AbstractTypeComponent<T extends AbstractActiveType> implements OnInit, OnDestroy {
  @ViewChildren(SortableHeaderDirective)
  public headers: QueryList<SortableHeaderDirective<T>>;
  public state: State<T> = {
    page: 1,
    pageSize: 4,
    searchTerm: '',
    sortColumn: this.doGetColumns()[0].key,
    sortDirection: ''
  };
  public typeForm: FormGroup;
  public searchTermControl: AbstractControl;
  public pageSizeControl: AbstractControl;
  public columns: Observable<Column<T>[]>;
  public types: Observable<T[]>;
  public totalTypes: Observable<number>;
  public total: Observable<number>;
  public alert: string = undefined;
  public success: string = undefined;
  public warning: string = undefined;
  public addButton = true;
  public deleteButton = true;
  public editButton = true;
  public refreshButton = true;
  public saveButton = true;
  public searchButton = true;
  public statusButton = true;
  public showTitle = true;
  public updates = 0;
  public key = getObjectKey;
  public typeSearch: T;

  constructor(
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    private typeService: TypeService<T>,
    private typeServiceTopic: TypeTopicService,
    private type: T,
    public title: string,
  ) {
    this.typeSearch = deepClone(type);
    this.columns = new BehaviorSubject<Column<T>[]>(this.doGetColumns());
  }

  public abstract doGetColumns(): Column<T>[];

  public async ngOnInit(): Promise<void> {
    this.typeForm = this.formBuilder.group({
      searchTerm: [this.state.searchTerm, []],
      pageSize: [this.state.pageSize, []],
    });
    this.searchTermControl = this.typeForm.controls.searchTerm;
    this.pageSizeControl = this.typeForm.controls.pageSize;
    await this.refreshAll();
    await this.connect();
  }

  public async refreshAll(): Promise<void> {
    await this.onSearchTermChange();
  }

  public async updateStatus(type: T): Promise<void> {
    const clone = deepClone(type);
    clone.active = !clone.active;
    await this.save(clone);
  }

  private async findAll(): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.typeService.findAll(this.state.page, this.state.pageSize, this.state.sortDirection, [this.state.sortColumn], this.typeSearch)
        .subscribe((response: HttpResponse<any>) => {
          if (!!response?.body && !!response?.headers?.get && !!response?.headers?.get('X-Total-Count')) {
            return resolve({
              total: response.headers.get('X-Total-Count'),
              types: response.body
            });
          } else {
            return reject({
              error: {
                message: 'Couldn\'t process your request, invalid response from backend.'
              },
            });
          }
        }, (error) => reject(error));
    }).then((data: { total: number, types: T[] }) => {
      this.types = new BehaviorSubject<T[]>(data.types);
      this.total = new BehaviorSubject<number>(data.total);
      this.totalTypes = new BehaviorSubject<number>(data.types.length);
      this.updates = 0;
      if (+data.total === 0 && !this.success && !this.alert) {
        this.warning = $localize`:@@findAll:No results found`;
      }
      setTimeout(() => {
        this.warning = undefined;
        this.success = undefined;
        this.alert = undefined;
      }, 2000);
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }

  private async connect(): Promise<void> {
    return new Promise<any>((resolve) => {
      resolve(this.typeServiceTopic.connect('/' + this.type.dtype, (message: Message) => {
        this.update(JSON.parse(message.body));
        message.ack();
      }));
    });
  }

  private update(typeTopic: TypeTopic<T>): void {
    switch (typeTopic.action) {
      case 'DELETE':
        this.types.subscribe(types => {
          const index: number = types.findIndex(t => t.id === typeTopic.type.id);
          if (index >= 0) {
            types.splice(index, 1);
          } else {
            this.updates++;
          }
        });
        break;
      case 'DELETE_ALL':
        this.types.subscribe(types => {
          typeTopic.types.forEach(type => {
            const index: number = types.findIndex(t => t.id === type.id);
            if (index >= 0) {
              types.splice(index, 1);
            } else {
              this.updates++;
            }
          });
        });
        break;
      case 'SAVE':
        this.types.subscribe(types => {
          const index: number = types.findIndex(t => t.id === typeTopic.type.id);
          if (index >= 0) {
            types[index] = typeTopic.type;
          } else {
            this.updates++;
          }
        });
        break;
      case 'SAVE_ALL':
        this.types.subscribe(types => {
          typeTopic.types.forEach(type => {
            const index: number = types.findIndex(t => t.id === type.id);
            if (index >= 0) {
              types[index] = type;
            } else {
              this.updates++;
            }
          });
        });
        break;
      case 'NONE':
      default: break;
    }
  }

  public async ngOnDestroy(): Promise<void> {
    await this.disconnect();
  }

  private async disconnect(): Promise<void> {
    return new Promise<any>((resolve) => {
      resolve(this.typeServiceTopic.disconnect(() => {
        console.log('Disconnected from Topic');
      }));
    });
  }

  public async onSearchTermChange(): Promise<void> {
    this.state.searchTerm = this.searchTermControl.value;
    this.columns.subscribe(columns => {
      columns.filter(column => column.type === 'string').forEach(column => {
        this.typeSearch[column.key] = this.searchTermControl.value;
      });
      columns.filter(column => column.type === 'number').forEach(column => {
        this.typeSearch[column.key] = +this.searchTermControl.value;
      });
    });
    await this.findAll();
  }

  public async onSort({ column, direction }: SortEvent<T>) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });
    this.state.sortColumn = column;
    this.state.sortDirection = direction;
    await this.refreshAll();
  }

  public async onPageChange(pageNumber: number): Promise<void> {
    this.state.page = pageNumber;
    await this.refreshAll();
  }

  public async onPageSizeChange(): Promise<void> {
    this.state.pageSize = +this.pageSizeControl.value;
    await this.refreshAll();
  }

  private async save(type: T): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.typeService.save(type)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then(async () => {
      this.success = $localize`:@@save:Successfully saved`;
      await this.refreshAll();
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }

  public async showDetails(type?: T): Promise<void> {
    const clone: T = deepClone(!type ? this.type : type);
    const ngbModalRef: NgbModalRef = this.modalService.open(ModalComponent, { backdrop: 'static', size: 'xl' });
    ngbModalRef.componentInstance.type = clone;
    ngbModalRef.componentInstance.title = this.title;
    ngbModalRef.componentInstance.saveButton = this.saveButton;
    ngbModalRef.componentInstance.typeService = this.typeService;
    ngbModalRef.componentInstance.componentRef = this.doGetModal();
    ngbModalRef.result.then(async (result) => {
      if (result === 'Save click') {
        this.success = $localize`:@@showDetails:Successfully saved`;
        await this.refreshAll();
      }
    });
  }

  public abstract doGetModal(): Type<Modal<T>>;

  public checked(): boolean {
    if (!!this.types) {
      let value = true;
      this.types.subscribe(types => { value = types.filter(type => type.checked).length === 0; });
      return value;
    }
    return true;
  }

  public async confirmDeleteAll(): Promise<void> {
    this.types.subscribe(types => {
      const clones: T[] = deepClone(types.filter(type => type.checked));
      const ngbModalRef: NgbModalRef = this.modalService.open(TypeConfirmDeleteModalComponent);
      ngbModalRef.componentInstance.types = clones;
      ngbModalRef.componentInstance.title = this.title;
      ngbModalRef.result.then(async (result) => {
        if (result === 'Ok click') {
          await this.deleteAll(clones);
        }
      });
    });
  }

  private async deleteAll(types: T[]): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.typeService.deleteAll(types)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then(async () => {
      this.success = $localize`:@@deleteAll:Successfully deleted all selected items`;
      await this.refreshAll();
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }

  public async confirmDelete(type: T): Promise<void> {
    const clone: T = deepClone(type);
    const ngbModalRef: NgbModalRef = this.modalService.open(TypeConfirmDeleteModalComponent);
    ngbModalRef.componentInstance.type = { ...type };
    ngbModalRef.componentInstance.title = this.title;
    ngbModalRef.result.then(async (result) => {
      if (result === 'Ok click') {
        await this.delete(clone);
      }
    });
  }

  private async delete(type: T): Promise<void> {
    return new Promise<any>((resolve, reject) => {
      this.typeService.delete(type)
        .subscribe((response) => resolve(response), (error) => reject(error));
    }).then(async () => {
      this.success = $localize`:@@delete:Successfully deleted`;
      await this.refreshAll();
    }).catch((error: HttpErrorResponse) => {
      if (error?.error?.message) {
        this.alert = error.error.message;
      } else {
        this.alert = error?.message;
      }
    });
  }
}
