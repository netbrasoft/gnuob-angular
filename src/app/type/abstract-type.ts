export abstract class AbstractType implements Object {
  id: number;
  version: number;
  dtype: string;

  constructor(dtype: string) {
    this.dtype = dtype;
  }
}
