import { SortableHeaderDirective } from './sortable-header.directive';
import { AbstractType } from '../abstract-type';

describe('SortableHeaderDirective', () => {
  it('should create an instance', () => {
    const directive = new SortableHeaderDirective<AbstractType>();
    expect(directive).toBeTruthy();
  });
});
