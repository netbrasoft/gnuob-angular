import { Directive, EventEmitter, Input, Output, HostListener, HostBinding, Type } from '@angular/core';
import { AbstractType } from '../abstract-type';

export type SortColumn<T extends AbstractType> = keyof T | any;
export type SortDirection = 'asc' | 'desc' | '';
const rotate: { [key: string]: SortDirection } = { asc: 'desc', desc: '', '': 'asc' };

export interface SortEvent<T extends AbstractType> {
  column: SortColumn<T>;
  direction: SortDirection;
}

export interface Column<T extends AbstractType> {
  key: SortColumn<T>;
  type: string;
  title: string;
}

export interface State<T extends AbstractType> {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: SortColumn<T>;
  sortDirection: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
})
export class SortableHeaderDirective<T extends AbstractType> {
  @Input() sortable: SortColumn<T> = '';
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent<T>>();
  @HostBinding('class.asc') get asc() {
    return this.direction === 'asc';
  }
  @HostBinding('class.desc') get desc() {
    return this.direction === 'desc';
  }
  @HostListener('click') rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }

  constructor() { }
}
