import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appTypeComponent]'
})
export class TypeComponentDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
