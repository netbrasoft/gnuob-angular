import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appTypeModalComponent]'
})
export class TypeModalComponentDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
