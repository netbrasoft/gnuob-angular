import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { AbstractType } from './abstract-type';
import { LocalTypeService } from './local-type.service';

describe('LocalTypeService', () => {
  let service: LocalTypeService<AbstractType>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ]
    });
    service = TestBed.inject(LocalTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
