import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AbstractType } from './abstract-type';
import { TypeFilter } from './type-filter';
import { TypeService } from './type-service';
import { HttpResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LocalTypeService<T extends AbstractType> implements TypeService<T> {
  private data: T[] = [];

  delete(type: T): Observable<any> {
    this.data = this.data.splice(this.data.findIndex(r => r.id === type.id));
    return of(new HttpResponse());
  }

  deleteAll(types: T[]): Observable<any> {
    types.forEach(t => this.delete(t));
    return of();
  }

  save(type: T): Observable<any> {
    if (!!this.data.find(t => t.id === type.id)) {
      this.delete(type);
    }
    this.data.push(type);
    return of(type);
  }

  saveAll(types: T[]): Observable<any> {
    types.forEach(t => this.save(t));
    return of(types);
  }

  findOne(type: T): Observable<any> {
    return of(this.data.find(t => t.id === type.id));
  }

  findAll(page: number, size: number, direction: string, properties: string[], type: T): Observable<any> {
    return of({
      headers: [{
        'X-Total-Count': this.data.length,
      }],
      body: this.data
    });
  }

  findAllByFilter(page: number, size: number, direction: string, properties: string[], filter: TypeFilter<T>): Observable<any> {
    return of({
      headers: [{
        'X-Total-Count': this.data.length,
      }],
      body: this.data
    });
  }
}
