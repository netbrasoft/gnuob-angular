import { Question } from './question';

export class CheckboxQuestion extends Question<boolean>{
  controlType = 'checkbox';

  constructor(options: {} = {}) {
    super(options);
  }
}
