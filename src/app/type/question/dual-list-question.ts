import { Question } from './question';

export class DualListQuestion extends Question<string>{
  controlType = 'duallist';
  filter: boolean;
  source: any[];
  destination: any[];

  constructor(options: {} = {}) {
    super(options);
    this.filter = options['filter'] || false;
    this.source = options['source'] || [];
    this.destination = options['destination'] || [];
  }
}