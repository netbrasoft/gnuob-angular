import { Injectable } from '@angular/core';
import { Question } from './question';
import { FormControl, Validators, FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class QuestionControlService {
  constructor() { }

  toFormGroup(questions: Question<string>[]) {
    const group: any = {};
    questions.forEach(question => {
      group[question.key] = new FormControl(question.value, question.validators);
    });
    return new FormGroup(group);
  }
}
