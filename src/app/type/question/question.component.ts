import { Component, ComponentRef, Input, OnDestroy, Type, ViewChild, OnInit, ComponentFactoryResolver } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { TypeComponentDirective } from '../directive/type-component.directive';
import { Question } from './question';
import { AbstractTypeComponent } from '../abstract-type.component';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.sass']
})
export class QuestionComponent implements OnInit, OnDestroy {
  @ViewChild(TypeComponentDirective, { static: true })
  private typeComponentDirective: TypeComponentDirective;
  private component: ComponentRef<Type<AbstractTypeComponent<any>>>;
  @Input()
  public question: Question<any>;
  @Input()
  public typeForm: FormGroup;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
  ) { }

  public ngOnInit(): void {
    if (this.question.controlType === 'template') {
      this.show(this.question.componentRef);
    }
  }

  public onCheckIfEmpty(): void {
    if (this.typeForm.controls[this.question.key].value === '') {
      this.typeForm.controls[this.question.key].setValue(null);
    }
  }

  public onSelectedChange(items: any[]): void {
    const data: any[] = [];
    items.forEach(i => {
      data.push(i);
    });
    this.typeForm.controls[this.question.key].setValue(data);
  }

  public onFilterChange(event: any): void {
    console.log(event);
  }

  private show(componentRef: Type<any>): void {
    this.ngOnDestroy();
    this.typeComponentDirective.viewContainerRef.clear();
    this.component = this.typeComponentDirective.viewContainerRef
      .createComponent(this.componentFactoryResolver.resolveComponentFactory(componentRef));
  }

  public ngOnDestroy(): void {
    if (!!this.component) {
      this.component.destroy();
    }
  }
}
