import { Type } from '@angular/core';
import { ValidatorFn } from '@angular/forms';
import { AbstractTypeComponent } from '../abstract-type.component';

export class Question<T> {
  value: T;
  componentRef: Type<AbstractTypeComponent<any>>;
  key: string;
  label: string;
  mask: string;
  required: boolean;
  validators: ValidatorFn[];
  placeholder: string;
  order: number;
  controlType: string;
  type: string;
  length: number;
  size: number;
  options: { key: string, value: string }[];
  filter: boolean;
  source: any[];
  destination: any[];

  constructor(options: {
    value?: T,
    componentRef?: Type<AbstractTypeComponent<any>>,
    key?: string,
    label?: string,
    mask?: string,
    required?: boolean;
    validators?: ValidatorFn[];
    placeholder?: string,
    order?: number,
    controlType?: string,
    type?: string,
    length?: number,
    size?: number,
  } = {}) {
    this.value = options.value;
    this.componentRef = options.componentRef;
    this.key = options.key || '';
    this.label = options.label || '';
    this.mask = options.mask || '';
    this.required = options.required || false;
    this.validators = options.validators;
    this.placeholder = options.placeholder || '';
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
    this.type = options.type || '';
    this.length = options.length === undefined ? 100 : options.length;
    this.size = options.size === undefined ? 12 : options.size;
  }
}