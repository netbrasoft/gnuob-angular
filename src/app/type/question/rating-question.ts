import { Question } from './question';

export class RatingQuestion extends Question<number>{
  controlType = 'rating';

  constructor(options: {} = {}) {
    super(options);
  }
}
