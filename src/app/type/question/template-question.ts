import { Type } from '@angular/core';
import { AbstractTypeComponent } from '../abstract-type.component';
import { Question } from './question';

export class TemplateQuestion extends Question<any> {
  controlType = 'template';
  componentRef: Type<AbstractTypeComponent<any>>;

  constructor(options: {} = {}) {
    super(options);
    this.componentRef = options['componentRef'] || undefined;
  }
}