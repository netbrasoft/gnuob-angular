import { Question } from './question';

export class TreeviewQuestion extends Question<string>{
  controlType = 'treeview';
  options: any;
  source: any[];

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || {};
    this.source = options['source'] || [];
  }
}
