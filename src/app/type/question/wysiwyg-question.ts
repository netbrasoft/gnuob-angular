import { Question } from './question';

export class WysiwygQuestion extends Question<string>{
  controlType = 'wysiwyg';
  options: any;

  constructor(options: {} = {}) {
    super(options);
    this.options = options['options'] || {};
  }
}
