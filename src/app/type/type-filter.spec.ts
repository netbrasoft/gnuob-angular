import { AbstractType } from './abstract-type';
import { TypeFilter } from './type-filter';

describe('UserFilter', () => {
  it('should create an instance', () => {
    expect(new TypeFilter<AbstractType>()).toBeTruthy();
  });
});
