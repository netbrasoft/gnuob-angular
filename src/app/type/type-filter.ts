import { AbstractType } from './abstract-type';

export class TypeFilter<T extends AbstractType> {
  from: string;
  to: string;
  type: T;
}
