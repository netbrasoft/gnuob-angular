import { AbstractType } from './abstract-type';
import { TypeFilter } from './type-filter';
import { Observable } from 'rxjs';

export interface TypeService<T extends AbstractType> {
  delete(type: T): Observable<any>;
  deleteAll(types: T[]): Observable<any>;
  save(type: T): Observable<any>;
  saveAll(types: T[]): Observable<any>;
  findOne(type: T): Observable<any>;
  findAll(page: number, size: number, direction: string, properties: string[], type: T): Observable<any>;
  findAllByFilter(page: number, size: number, direction: string, properties: string[], filter: TypeFilter<T>): Observable<any>;
}
