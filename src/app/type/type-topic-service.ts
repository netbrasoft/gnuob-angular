import { Message } from 'webstomp-client';

export interface TypeTopicService {
  connect(path: string, callback?: (message: Message) => any): void;
  disconnect(disconnectCallback: () => any): void;
}
