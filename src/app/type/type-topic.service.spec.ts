import { TestBed } from '@angular/core/testing';

import { TypeTopicService } from './type-topic.service';

describe('TypeTopicService', () => {
  let service: TypeTopicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypeTopicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
