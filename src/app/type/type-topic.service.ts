import { Injectable } from '@angular/core';
import { Client, client, Message, Subscription } from 'webstomp-client';
import { environment } from '../../environments/environment';
import { User } from '../authentication/user/user';

@Injectable({
  providedIn: 'root'
})
export class TypeTopicService implements TypeTopicService {
  private client: Client;
  private subscription: Subscription;
  private user: User = JSON.parse(localStorage.getItem('user'));

  constructor() { }

  public connect(path: string, callback?: (message: Message) => any): void {
    this.init();
    this.client.connect(this.user?.username, this.user?.token, () => {
      this.subscription = this.client.subscribe('/topic' + path, callback);
    }, (error) => {
      console.error(error);
    });
  }

  private init(): void {
    this.client = client(environment.endpointWebSockets + '/gs-guide-websocket', {
      debug: !environment.production,
      protocols: ['v10.stomp', 'v11.stomp', 'v12.stomp'],
      binary: false,
      heartbeat: { incoming: 10000, outgoing: 10000 }
    });
  }

  public disconnect(disconnectCallback: () => any): void {
    try {
      if (this.subscription != null) {
        this.client.unsubscribe(this.subscription.id);
      }
      this.client.disconnect(disconnectCallback);
    } catch (error) {
      console.error(error);
    }
  }
}
