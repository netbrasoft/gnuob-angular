import { AbstractType } from './abstract-type';

export class TypeTopic<T extends AbstractType> {
  action: string;
  type: T;
  types: T[];
}
