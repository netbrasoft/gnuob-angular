import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { TypeService } from './type.service';
import { AbstractType } from './abstract-type';

describe('UserService', () => {
  let service: TypeService<AbstractType>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
      ]
    });
    service = TestBed.inject(TypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
