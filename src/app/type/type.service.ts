import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, retry } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { AbstractType } from './abstract-type';
import { TypeFilter } from './type-filter';

@Injectable({
  providedIn: 'root'
})
export class TypeService<T extends AbstractType> implements TypeService<T> {
  constructor(private httpClient: HttpClient) { }

  public delete(type: T): Observable<HttpResponse<T>> {
    return this.httpClient.request<HttpResponse<T>>('DELETE', environment.endpoint + '/' + type.dtype, {
      headers: new HttpHeaders().set('Authorization', localStorage.getItem('token')),
      body: type
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public deleteAll(types: T[]): Observable<HttpResponse<T>> {
    return this.httpClient.request<HttpResponse<T>>('DELETE', environment.endpoint + '/' + types[0].dtype + '/all', {
      headers: new HttpHeaders().set('Authorization', localStorage.getItem('token')),
      body: types
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public save(type: T): Observable<HttpResponse<T>> {
    return this.httpClient.post<HttpResponse<T>>(environment.endpoint + '/' + type.dtype, type, {
      headers: new HttpHeaders().set('Authorization', localStorage.getItem('token')),
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public saveAll(types: T[]): Observable<HttpResponse<T>> {
    return this.httpClient.post<HttpResponse<T>>(environment.endpoint + '/' + types[0].dtype + '/all', types, {
      headers: new HttpHeaders().set('Authorization', localStorage.getItem('token')),
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public findOne(type: T): Observable<HttpResponse<T>> {
    return this.httpClient.post<HttpResponse<T>>(environment.endpoint + '/' + type.dtype + '/of/one', type, {
      headers: new HttpHeaders().set('Authorization', localStorage.getItem('token')),
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public findAll(page: number, size: number, direction: string, properties: string[], type: T): Observable<HttpResponse<any>> {
    return this.httpClient.post<HttpResponse<any>>(environment.endpoint + '/' + type.dtype + '/of/all', type, {
      headers: new HttpHeaders().set('Authorization', localStorage.getItem('token')),
      observe: 'response',
      params: {
        page: String(page - 1),
        size: String(size),
        direction: direction.toUpperCase(),
        properties: properties,
      }
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }

  public findAllByFilter(page: number, size: number, direction: string, properties: string[], filter: TypeFilter<T>): Observable<HttpResponse<any>> {
    return this.httpClient.post<HttpResponse<any>>(environment.endpoint + '/' + filter.type.dtype + '/of/all/filter', filter, {
      headers: new HttpHeaders().set('Authorization', localStorage.getItem('token')),
      observe: 'response',
      params: {
        page: String(page - 1),
        size: String(size),
        direction: direction.toUpperCase(),
        properties: properties,
      }
    }).pipe(
      retry(3),
      map(response => {
        return response;
      })
    );
  }
}
