import { ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';
import { Category } from './category/category';
import { TreeviewItem, TreeItem } from 'ngx-treeview';
import { Attribute } from './product/attribute/attribute';

export const passwordMismatchValidator: ValidatorFn = (control: FormGroup): ValidationErrors | null => {
  const passwordControl = control.get('password');
  const repeatPasswordControl = control.get('repeatPassword');
  return passwordControl && repeatPasswordControl
    && passwordControl.value !== repeatPasswordControl.value ? { passwordMismatch: true } : null;
};

export const getObjectKey = (object: any, key: string): any => {
  let obj = object;
  try {
    key.split('.').forEach((k: string) => {
      obj = obj[k];
    });
    return obj;
  } catch (error) {
    return obj;
  }
};

export const deepClone = (obj: any) => {
  let copy: any;
  // Handle the 3 simple types, and null or undefined
  if (null == obj || 'object' !== typeof obj) {
    return obj;
  }
  // Handle Date
  if (obj instanceof Date) {
    copy = new Date();
    copy.setTime(obj.getTime());
    return copy;
  }
  // Handle Array
  if (obj instanceof Array) {
    copy = [];
    for (let i = 0, len = obj.length; i < len; i++) {
      copy[i] = deepClone(obj[i]);
    }
    return copy;
  }
  // Handle Object
  if (obj instanceof Object) {
    copy = {};
    for (const attr in obj) {
      if (obj.hasOwnProperty(attr)) {
        copy[attr] = deepClone(obj[attr]);
      }
    }
    return copy;
  }
  throw new Error('Unable to copy obj! Its type isn\'t supported.');
};

export const getCategoryTreeviewItems = (categories: Category[], checkedCategories: Category[]): TreeviewItem[] => {
  const treeItems: TreeItem[] = [];
  categories.forEach((category: Category) => {
    treeItems.push({
      text: category.name,
      value: category,
      checked: !!checkedCategories.find(c => c.id === category.id),
      children: []
    });
  });
  treeItems.forEach((treeItem: TreeItem) => {
    if (!!treeItem.value.parent) {
      const parent: TreeItem = treeItems.find(c => c.value.id === treeItem.value.parent.id);
      parent.children.push(treeItem);
    }
  });
  const treeviewItems: TreeviewItem[] = [];
  treeItems.filter(treeItem => !treeItem.value.parent).forEach(treeItem => treeviewItems.push(new TreeviewItem(treeItem, true)));
  return treeviewItems;
};

export const getAttributeTreeviewItems = (attributes: Attribute[], checkedAttributes: Attribute[]): TreeviewItem[] => {
  const treeItems: TreeItem[] = [];
  attributes.forEach((attribute: Attribute) => {
    treeItems.push({
      text: attribute.name,
      value: attribute,
      checked: !!checkedAttributes.find(a => a.id === attribute.id),
      children: []
    });
  });
  treeItems.forEach((treeItem: TreeItem) => {
    if (!!treeItem.value.parent) {
      const parent: TreeItem = treeItems.find(c => c.value.id === treeItem.value.parent.id);
      parent.children.push(treeItem);
    }
  });
  const treeviewItems: TreeviewItem[] = [];
  treeItems.filter(treeItem => !treeItem.value.parent).forEach(treeItem => treeviewItems.push(new TreeviewItem(treeItem, true)));
  return treeviewItems;
};
