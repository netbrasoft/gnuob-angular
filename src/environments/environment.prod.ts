export const environment = {
  production: true,
  name: require('../../package.json').name,
  version: require('../../package.json').version,
  endpoint: window.location.protocol + '//' + window.location.hostname + ':' + window.location.port,
  endpointWebSockets: 'ws://' + window.location.hostname + ':' + window.location.port,
};
