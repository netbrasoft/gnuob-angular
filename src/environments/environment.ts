
export const environment = {
  production: false,
  name: require('../../package.json').name,
  version: require('../../package.json').version,
  endpoint: window.location.protocol + '//' + window.location.hostname + ':8000',
  endpointWebSockets: 'ws://' + window.location.hostname + ':8000',
};
